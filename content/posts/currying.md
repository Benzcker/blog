+++ 
date = 2023-01-07T09:22:59+01:00
title = "Currying - Next Level Coding"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "IT", "Coding", "Funktionale Programmierung" ]
categories = []
externalLink = ""
series = []
+++

Die Kunst des Programmierens ist ein Eisberg, der sehr weit unter die Wasseroberfläche reicht. 
Ich erinnere mich daran, dass ich schon vor Jahren dachte, ich hätte die wichtigsten Techniken des Programmierens gemeistert und könne keine großen Überraschungen mehr erleben. 
Wie sehr ich mich getäuscht hatte.

Eine Technik hat es mir besonders angetan: das Currying. Wer dies nicht beherrscht, dem fehlt ein essentielles Werzeug in seinem Werkzeugkoffer.

<!--more-->

![Curry](/currying/curry.jpg) 

# Was ist Currying?
Currying beschreibt die partielle Anwendung von Funktionen. 
Genauer gesagt werden Funktionen mit mehreren Parametern in eine Reihe von Funktionen mit je nur einem Argument umgewandelt.
Auf diese Weise können einzelne Parameter einer Funktionen zu einem Zeitpunkt, die restlichen Parameter zu einem späteren Zeitpunkt gesetzt werden. 
In der Praxis nutzt man beim Currying Funktionen, welche als Rückgabewert wiederum neue Funktionen haben.
Eine der Sprachen, in denen Currying häufig eingesetzt wird, ist JavaScript.

Schauen wir uns folgendes Problem an:

{{< highlight javascript >}}
function hello(name) {
  console.log("Hallo " + name);
}
function compliment(name) {
  console.log("Gut siehst du aus, " + name);
}
function bye(name) {
  console.log("Auf Wiedersehen, " + name);
}
{{< /highlight >}}

Für mich sieht das sehr WET aus. Früher hätte ich das vielleicht so gelöst:

{{< highlight javascript >}}
function print(msg, name) {
  console.log(msg + name);
}
{{< /highlight >}}

Das kann allerdings auch schnell WET werden.

{{< highlight javascript >}}
const HELLO = "Hallo ";
print(HELLO, "Tom");
print(HELLO, "Sebastian");
print(HELLO, "Albert");
print(HELLO, "Fritz");
print(HELLO, "Georg");
{{< /highlight >}}


An sich sieht das schon ganz sauber aus, nur der erste Parameter wiederholt sich oft. Um das zu vermeiden, können wir nun Currying verwenden:

{{< highlight javascript >}}
function createMsg(msg) {
  return function(name) {
    console.log(msg + name);
  }
}
{{< /highlight >}}

Hier haben wir die Funktion print mit 2 Parametern aufgeteilt, sodass wir zwei Funktionen haben, die jeweils nur einen Parameter annehmen.
Nun können wir den ersten Parameter einzeln anwenden und die erzeugte Funktion wiederholt verwenden.

{{< highlight javascript >}}
const hello = createMsg("Hallo ");

hello("Tom");
hello("Sebastian");
hello("Albert");
hello("Fritz");
hello("Georg");
{{< /highlight >}}

# Praxisbeispiel
Erst neulich ärgerte ich mich, weil ich zwei nahezu identische Funktionen exportieren wollte, die sich einzig an einer Stelle unterschieden. 
Ich hatte eine Funktion geschrieben, die bei einem Zugriff auf einen NodeJS Server überprüft, ob der Nutzer einen gültigen JSON Webtoken hat.
Das ganze hatte ich als Middleware entworfen, um so bestimmte API-Zugriffe zu schützen.
Nun wollte ich gerne die gleiche Funktion nutzen, um manche Seiten zu schützen. Der einzige Unterschied sollte sein, dass in diesem Fall kein Status 403 im Falle
eines Fehlers gesendet werden, sondern der Nutzer stattdessen zur Loginseite weitergeleitet werden sollte.
Mittels Currying konnte ich das Problem schnell lösen, ohne einen Codeblock kopieren zu müssen.

In Zeile 24 und 25 wird die Unterscheidung zwischen Weiterleitung zum Login und Senden eines Fehlercodes gemacht.
Der Rest des Codes ist für das Verständnis von Currying nicht notwendig, allerdings zum Zwecke der Vollständigkeit dargestellt.

{{< highlight typescript "linenos=table,linenostart=17" >}}
const createIsLoggedIn = (isFrontend: boolean) => 
{
  return (req: Request, res: Response, next: NextFunction) => 
  {
    let token = req.headers['x-access-token'] as string | undefined;
    if (!token) 
    {
      if (isFrontend) res.redirect('/login?fwd=' + req.baseUrl);
      else res.status(403).send('Nicht angemeldet!');
      return;
    }

    jwt.verify(token, SECRET, (err, decoded) => 
    {
      try 
      {
        if (err) throw err; 
        req.userId = (decoded as TokenContent).userId; 
      }
      catch (e)
      {
        res.status(401).send('Nicht autorisiert!');
        return;
      }
      next();
    });
  };
};

export const isLoggedInFrontend = createIsLoggedIn(true);
export const isLoggedIn = createIsLoggedIn(false);
{{< /highlight >}}

Wenn man es genau nimmt, müsste für Currying auch die zweite Funktion aufgebrochen werden, sodass sie nur ein Argument nimmt und so weiter, aber das war in diesem Fall weder nötig noch möglich.


# Funktionale Programmierung
Für Objekt-orientierte Programmierer mag Currying ungewöhnlich sein, in der funktionalen Programmierung hingegen ist es derart selbstverständlich, 
dass in F# beispielsweise jede Funktion schon beim Erstellen gecurryied wird.

{{< highlight fsharp >}}
let add x y = x + y // Funktion, die zwei Zahlen addiert

let five = add 2 3 // = 5

let addFour = add 4 // Neue Funktion, die immer 4 addiert
let six = addFour 2 // = 6
{{< /highlight >}}

Wieso das funktioniert wird ersichtlich, wenn wir uns die automatisch generierten Typen der Funktionen in F# ansehen:

{{< highlight fsharp >}}
let add x y = x + y // int -> int -> int

let five = add 2 3 // int

let addFour = add 4 // int -> int
let six = addFour 2 // int
{{< /highlight >}}

Eine Funktion mit 2 Parametern ist in Wirklichkeit eine Funktion mit nur einem Parameter, die eine Funktion zurück gibt, die den zweiten Parameter annimmt und schließlich den finalen Wert zurück gibt.

Das ist sehr praktisch. In folgendem Beispiel konnte ich davon Gebrauch machen.
Ich erledigte im Rahmen meines Studiums eine Aufgabe auf [exercism.org](https://exercism.org). Die Aufgabe lautete: 
[_"Finde die Summe aller eindeutigen Vielfachen bestimmter Zahlen bis zu dieser Zahl, aber nicht einschließlich dieser Zahl."_](https://exercism.org/tracks/fsharp/exercises/sum-of-multiples)

Mit einfachen Worten: Man bekommt eine Liste von Zahlen, deren Vielfache addiert werden sollen. Die Vielfache sollen allerdings nicht höher als eine weitere angegebene Zahl werden.
Beispiel: Man bekommt die Zahlen 2 und 5 und das Maximum ist 7, dann sollen die Zahlen 2, 4, 5 und 6 addiert werden. Das Ergebnis wäre 17.

Ich schrieb die Funktion multiples, die die Vielfachen einer Zahl unter einem Maximum berechnet. Das Maximum müsste ich normalerweise jedes Mal mit angeben.
Da in F# Currying der Standard ist, habe ich stattdessen eine neue Funktion multiplesUnderBound erstellt, die ich 3 Zeilen tiefer in einem List.map verwenden kann. 
Auf diese Weise ist der Code angenehm DRY und prägnant.

{{< highlight fsharp >}}
module SumOfMultiples

let multiples upperBound number = [ 0 .. number .. upperBound-1 ]
let notZero n = n <> 0
let sum (numbers: int list) (upperBound: int): int =
  let multiplesUnderBound = multiples upperBound
  numbers
  |> List.filter notZero
  |> List.map multiplesUnderBound
  |> List.map Set.ofList
  |> Set.unionMany
  |> Seq.sum
{{< /highlight >}}

# Abschließende Gedanken
Grundvorraussetzung für Currying ist, dass in der gewählten Sprache während der Laufzeit Funktionen definiert werden können.
Das ist beispielsweise in JavaScript und Python der Fall.

Andererseits gibt es auch in Sprachen wie C Überlegungen, wie Currying mit einigen Tricks angewandt werden kann.
Weitere Informationen dazu kann man beispielsweise in Damis Paper 
[_More Functional Reusability in C / C++ / Objective-c with Curried Functions: working paper_](https://archive-ouverte.unige.ch/unige:157921) lesen.

