+++ 
date = 2024-07-05T05:30:00+01:00
title = "Die S-Bahn ist gar nicht so unpünktlich, wie ihr denkt"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "Mathematik" ]
categories = []
externalLink = ""
series = []
+++

# Berliner S-Bahn im Schnitt 121 Mal pro Tag gestört
So berichten vor zwei Tagen rbb24[^1], stern[^2], tagesspiegel[^3] und co.
Aber ist das eine irreführende Zahl?
Ich gehe dem Ganzen auf den Grund.

![S5](/s-bahn/s5.png)

<!--more-->

[^1]: https://www.rbb24.de/panorama/beitrag/2024/07/s-bahn-berlin-pro-tag-121-stoerungen.html
[^2]: https://www.stern.de/gesellschaft/regional/berlin-brandenburg/nahverkehr--s-bahn-berlin--pro-tag-im-schnitt-121-stoerungen-34850690.html
[^3]: https://www.tagesspiegel.de/berlin/40000-zuge-ausgefallen-die-berliner-s-bahn-kommt-immer-ofter-zu-spat--oder-gar-nicht-11941457.html 

Zunächst klingen 121 Störungen am Tag ziemlich viel.
Wir wollen natürlich gar keine Störungen oder wenigstens nur alle paar Wochen mal eine.
Da erscheint es skandalös, dass es mehr als hundert Störungen am Tag geben soll, und das nur in Berlin!

Aber wir tappen dabei schnell in die Falle, dass wir die Verhältnisse, die in großen Maßstäben vorliegen, nicht einschätzen können.
Ich möchte das ganze an folgendem Beispiel erläutern.

## Parship
"Alle 11 Minuten verliebt sich ein Single über Parship", so lautet der Werbeslogan seit Jahren.
Das sind 131 Singles pro Tag, fast 50.000 frisch verliebtes Singles jedes Jahr.

$${24{h \over d}*60{min \over h} \over {11 {min \over Single}}} * 365,25 {d \over Jahr}$$
$$= 47.814,5\ {Singles \over Jahr}$$

Das klingt zunächst nach einer ganzen Menge, allerdings hat Parship 11 Millionen Nutzer[^4].
Dementsprechend ist die Wahrscheinlichkeit, dass sich ein bestimmter Nutzer über Parship in einem Jahr verliebt bei etwa 0,44%.

[^4]: https://www.leadingdatingsites.co.uk/reviews-dating-agencies/parship-uk.htm

$${47.814,5 \over 11.000.000} = 0,435\\%$$

Wenn wir wissen wollen, wie lange die durchschnittliche Wartezeit $T$ beträgt, bis man sich verliebt, müssen wir den Kehrwert der Wahrscheinlichkeit nehmen.

$$T=({0,00435 \over Jahr})^{-1}=229,9\ Jahre$$

Fast 230 Jahre muss ein Parship Nutzer also im Schnitt warten, bis er sich verliebt.
Ich bin sicher, dass Parship mit ihrem Werbeslogan etwas anderes ausdrücken wollte und dass man auf dieser Plattform in Wirklichkeit deutlich bessere Chancen hat.
Dennoch zeigt dieses mathematische Experiment, wie leicht wir mit solchen absoluten Zahlen getäuscht werden können.

## Was sagen uns also 121 Störungen am Tag?
Bei diesen Störungen handelt es sich um Notarzt-, Polizei- und Feuerwehr-Einsätze, Großveranstaltungen, Staatsbesuche und Störungen an Zügen und in der Infrastruktur[^5].

[^5]: https://sbahn.berlin/fahren/bauen-stoerung/gruende-fuer-stoerungen/

Fairerweise muss man sagen, dass die S-Bahn nur für die Störungen an Zügen und in der Infrastruktur Verantwortung trägt.
Alle diese Arten von Störungen haben aber auch gemeinsam, dass wir hier nicht wie bei Parship einen Referenzwert haben, von dem wir eine Prozentzahl berechnen können.

Wir können aber sagen, dass die S-Bahn in Berlin 168 Bahnhöfe bedient.
Die Störungen sind allerdings nicht auf je einen Bahnhof bezogen, eine Störung kann zwischen zwei Bahnhöfen auftreten und dann alle danachfolgenden betreffen.
Gehen wir davon aus, dass etwa 1.500 S-Bahn-Züge jeden Tag fahren.
Wenn jede Störung nur einen Zug betrifft, haben wir
${121 \over 1.500}=8,1\\%$
aller Züge, die von einer Störung betroffen sind.

Das passt auch in etwa zu der Pünktlichkeitsquote der S-Bahn.
2023 lag diese bei 93,6%[^1], also waren 6,4% der S-Bahnen unpünktlich.
Das ist nicht schön, immerhin ist das Ziel der S-Bahn mit mindestens 96% ihrer Züge pünktlich zu sein.
Pünktlichkeit definieren sie übrigens mit maximal 5 Minuten Verspätung.

Wenn allerdings 8,1% aller Züge von Störungen betroffen sind, ist es nicht verwunderlich, dass 6,4% der S-Bahnen unpünktlich sind.
Die Bahn schreibt selber, dass die Senkung des Störgeschehens die wichtigste Aufgabe bleibt, da es ab einem täglichen Störungsaufkommen von mehr als 100 (
${100 \over 1.500}=6,7\\%$
) kaum noch möglich sei, das Pünktlichkeitsziel von 96% zu erreichen.

Fährt ein Pendler mit zwei Zügen am Tag, hat er eine Wahrscheinlichkeit von 12,4%, dass mindestens einer der Züge verspätet ist.

$$P(X \ge 1) = 1 - P(X=0)$$
$$= 1 - 0,936^2 = 12,4\\%$$

Aus diesem Grund sollte er im Schnitt etwa alle 8 Tage mindestens einen verspäteten Zug auf seinem Arbeitsweg haben.

$$T={0,124 \over Tag}^{-1}=8,06\ Tage$$

Bei einer Arbeitswoche mit 5 Tagen entspricht das einer Verspätung alle 1,5 Wochen.
Und dies ist eine deutlich vielsagendere Angabe als die 121 Störungen jeden Tag, auch wenn wir gezeigt haben, dass diese Störungen vermutlich eng mit den Verspätungen zusammenhängen.

Nun bleibt nur noch die Frage, welcher Anteil der Störungen von der S-Bahn selbstverschuldet ist.

<br>
<br>
<small style="color: #555">
Dieser Artikel entstand aus einem meiner klassischen Gespräche mit Flo und Robin. 
Danke an die beiden für die Ideen!
</small>

<br>
<br>

Flo hatte noch folgende Anmerkungen zu diesem Artikel:

<i>
Der Absatz mit den 1500 Zügen pro Tag fühlt sich ein bisschen aus der Luft gegriffen an. Das passt nicht zum Rest deines Blogs der (angenehmerweise) fast alle wichtigen Aussagen mit Quellen belegt. 
Sowohl 1500 Züge als auch eine Störung betrifft nur einen Zug sind Annahmen, die ich skeptisch sehe. (Ich würde von mehr Zügen ausgehen und mehr betroffenen Zügen pro Störung) → ähnliches Ergebnis 

Ich lag mit meinen mehr als 1000 Zügen daneben. Gemäß https://sbahn.berlin/das-unternehmen/unternehmensprofil/auf-einen-blick-zahlen-und-fakten/ gibt es 1278 Fahrer. Wenn die ca. 17 Tage im Monat arbeiten ( 30 Tage - 8 Tage Wochenende - 5 Tage Urlaub & krankheit) müssen es deutlich unter 1000 Züge am Tag sein.

Beispiel s bahn ring: der s bahn ring hat 28 Stationen. Einmal rum dauert ca 1h. D.h. von einer Station zu nächsten im Schnitt 2m. Damit alle 5 Minuten eine sbahn kommen kann muss alle 2,5 Stationen eine sbahn eingesetzt sein. 
Wenn wir auf 30 Stationen runden und auf 1 Zug alle 3 Stationen aufrunden brauchen wir 10 Züge für den ring in die eine Richtung + 10 Züge Gegenrichtung. 20 Züge für 30 Stationen. Also Züge = Bahnhöfe * 2/3. (Die Formel nimmt allerdings an, dass alle s bahn Stationen nur < 2 Minuten Fahrt auseinander sind und alle im 5 Minuten takt bedient werden.)
Jedenfalls würde das für unsere 168 Stationen bedeuten, dass 112 Züge gleichzeitig im Einsatz sind.
Ab hier wird knifflig: \
Gehen wie vereinfachen davon aus, dass jede Störung unerwartet genau einen Zug trifft und dieser dadurch für den Rest seines Einsatzes verspätet wäre. Wenn ein Zug dauerhaft zu spät käme und 24/7 im Einsatz wäre, würde er ca 0.9% Verspätungen verursachen. Bei 6.4% unpünktlichkeit entspricht das 7.1 Zügen die dauerhaft unpünktlich fahren. 

121 Störungen am Tag bedeutet 5 Störungen die Stunde. Damit 5 Störungen die Stunde 7.1 dauerhaft verspätete Züge verursachen muss ein Zug der von einer Störung betroffen wurde nach 7.1/5 = 1.4 h ersetzt werden. 

Unter all diesen vereinfachenden Annahmen bedeutet das, dass die s bahn im Schnitt 1,4h braucht um einen Zug, der von einer Störung betroffen wurde zu ersetzen.\
In meinem Modell, um eine Pünktlichkeit von 96% zu erreichen müsste die sbahn diese Zeit auf 4%/(0.9*5) = 0.88h senken. Wenn die sbahn es also schaffen würde ihre gestörten Züge ca 30 Minuten schneller zu ersetzen, als sie es aktuell tun, würden sie ihr Pünktlichkeitsziel einhalten.

<br>

Mein Fazit: Ich glaube die Anzahl an Störungen auf die Pünktlichkeit zu mappen ist ohne viele weitere Faktoren zu berücksichtigen nicht seriös möglich.
Fakten die fehlen (bestimmt unvollständig): 
- Wie viele Züge sind im Einsatz
- Wie viele Züge sind von einer Störung betroffen
- Wie viele Störungen sind unerwartet 
- Wie viele Züge verspätet sich ohne von einer Störung betroffen zu sein
- wie viel Prozent Verspätung verursacht ein verspäteter Zug pro Station die er anfährt
</i>


