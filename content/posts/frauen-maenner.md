+++ 
date = 2024-09-26T07:00:00+01:00
title = "Frauen passen nicht zu Männern (lexikalisch)"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "Etymologie" ]
categories = []
externalLink = ""
series = []
+++

![Männer und Frauen](/maenner-frauen/couple.jpg)

<!--more-->

Wir haben unterschiedliche Wörter in der deutschen Sprache mit ähnlichen Bedeutungen. Und doch gibt es gewisse Nuancen, die es zu beachten sich lohnt. Denn über eine präzise und eindeutige Sprache kann Missverständnissen vorgebeugt und der allgemeine Sprachmehrwert erhöht werden.
Dazu muss man sich allerdings im Klaren sein, wo der genauen Unterschied zwischen den sich inhaltlich ähnelnden Worten liegt.

Nehmen wir das Wort "Frau". Es bezeichnet eine "erwachsene Person weiblichen Geschlechts"[^4], was meiner Meinung nach wenig aussagt.
Ich würde es als einen ausgewachsenen Menschen mit zwei X-Chromosomen definieren, wobei ich da von manchen Seiten Widerspruch bekäme.
Das passende Gegenstück zur Frau ist der Mann, eine "erwachsene Person männlichen Geschlechts"[^5], oder einfach ein ausgewachsener Mensch mit einem X- und einem Y-Chromosom.
Ein verwandtes Wortpaar ist die Dame und der Herr, es sind die höflichen Anreden an erwachsene Menschen.
Eine unverheiratete Frau war früher ein Fräulein, also eine verniedlichte Frau. Das ergibt Sinn, denn die meisten Frauen heirateten mit dem Eintritt in das Erwachsenenleben.
Im Umkehrschluss waren fast nur Minderjährige unverheiratet, daher die Verniedlichung.
Ein unverheirateter Mann müsste demnach ein Männlein sein. Dieses Wort ist allerdings noch weniger im Sprachgebrauch üblich, als das ohnehin schon wenig verbreitete Fräulein.
Das liegt vermutlich daran, dass es bei Männern deutlich üblicher war, als Erwachsener nicht sofort zu heiraten.
[^4]: https://www.dwds.de/wb/Frau 
[^5]: https://www.dwds.de/wb/Mann 

Wo passt eigentlich die Herrin in dieses Schema? Es ist die weibliche Variante von Herr, wie es z. B. auch im Lateinischen normal ist. 
Da ist das Gegenstück zum _dominus_ (lat. "Herr") die _domina_ (lat. "Herrin"). Aber dafür haben wir doch im Deutschen bereits die Dame.
Stattdessen beschreibt die Herrin im allgemeinen Verständnis eine Frau mit eher typisch männlichen Eigenschaften, meist gebieterisch und streng. 
Währenddessen lässt die Dame ein anmutiges, besonders weibliches Wesen vermuten.
Dies ist allerdings rein konnotativ, da die Dame vom französischen _dame_ kommt, was wiederum nur vom lateinischen _domina_ (lat. "Herrin") kommt[^6].
Die beiden Worte "Herrin" und "Dame" haben also dieselbe Bedeutung, nur einen anderen Ursprung.

Wir nehmen als passenden Gegensatz aber dennoch lieber die Dame zum Herren.
Es heißt ja auch "Sehr geehrte Damen und Herren".<br>
Übrigens kommen _dominus_ und _domina_ von _domus_ (lat. "Haus")[^6], weil es sich um die Hausbesitzer handelte.
[^6]: https://www.etymonline.com/de/word/dame

Ich bin Herr Zenz und meine Frau ist... Frau Zenz?
Aber wir haben doch gesagt, der passende Gegensatz zum Herren ist die Dame.
Warum sagen wir dann nicht 'Dame Zenz'? <br>
Tatsächlich war es vor nicht allzu langer Zeit üblich, Frauen mit gehobenem Status direkt mit Dame zu anzureden.
Und auch Herr stand ja ursprünglich für einen Mann von höherem Stand. Es leitet sich vom althochdeutschen Wort _hēriro_ ab, das "Anführer" oder "Herrscher" bedeutete. 
Dieses wiederum ist verwandt mit _herizogo_ (ahd. _heri_ Heer, _zogo_ ziehen, führen) für den Heerführer, aus dem später der Herzog wurde[^1].
Auch heute noch im Wort "Herrscher" finden wir wieder den Herren.
[^1]: https://www.koeblergerhard.de/ahd/ahd_h.html

Wenn wir nun aber gesellschaftlich dazu übergegangen sind, alle Männer wie eine Autoritätsperson mit Herr anzusprechen, warum sprechen wir dann Frauen nicht immer mit Dame an?
In der Tat war auch Frau früher eine ehrende Bezeichnung für Frauen von höherem Stand. Frau kommt von Althochdeutsch _frouwa_ und steht für "Herrscherin", "Gebieterin"[^2].
[^2]: https://www.koeblergerhard.de/ahd/ahd_f.html

Eine gewöhnliche Frau war ein Weib, ohne die negative Konnotation der heutigen Tage. Denn Weib kommt von _wīb_ und bedeutet einfach "Frau" oder "Mädchen"[^3].
[^3]: https://www.koeblergerhard.de/ahd/ahd_w.html

Also ist es doch durchaus richtig, wenn man Herr Zenz und Frau Zenz zusammenbringt. 
Aber von Mann und Frau sollte nicht die Rede sein, denn das lexikalische Gegenstück zum Mann ist eigentlich das Weib, 
und zum Herren die Frau oder die Dame.

