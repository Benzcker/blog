+++ 
date = 2024-05-05T12:00:00+01:00
title = "Jenseits der Stille - Wenn Neugeborene zu Helden werden"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "Gesundheit", "Zwerchfellhernie" ]
categories = []
externalLink = ""
series = []
+++

In den freudigen Momenten des Lebens, wenn neues Leben die Welt mit Glück erfüllt, finden sich die tiefsten Spuren von Liebe.
Doch die Freude ist schnell vorüber, wenn der Kampf um das Leben bereits mit der Geburt beginnt.

![Zwerchfellhernie bei Neugeborenen – CDH e.V.](/zwerchfellhernie/Logo-CDH.png)
<small>[©cdhev](https://www.umm.de/klinik-fuer-neonatologie/mitarbeiterinnen/ "Mitarbeiter:innen: Uniklinik Mannheim")</small>

<!--more-->

# Was ist eine angeborene Zwerchfellhernie?
Bei einer angeborenen Zwerchfellhernie handelt es sich um eine schwerwiegende Fehlbildung des Zwerchfells.

Das Zwerchfell (auch Diaphragma genannt) ist eine Platte aus Muskeln und Sehnen, die den Brustraum von der Bauchhöhle trennt.
Es hat etwa die Form einer Kuppel und wird zum Atmen und z.B. auch zum Lachen verwendet.
Bei einer Zwerchfellhernie entsteht ein Loch auf einer der beiden Seiten des Zwerchfells, oder das Zwerchfell wird auf dieser Seite gar nicht erst angelegt.
Die meisten Zwerchfellhernien entstehen auf der linken Seite, nur etwa 15% sind auf der rechten Seite. In ganz seltenen Fällen kann sie sogar auf beiden Seiten auftreten.

Die Fehlbildung entsteht meist in der achten bis zehnten Schwangerschaftswoche, wird aber häufig erst deutlich später entdeckt.
Nicht selten wird sie sogar erst nach der Geburt festgestellt.

Ist erst einmal das Loch im Zwerchfell, liegt das Hauptproblem darin, dass Organe aus der Bauchhöhle in die Brusthöhle verlagert werden und dabei die Lunge in ihrer Entwicklung stören. 
Dabei ist der Lungenflügel auf der Seite der Hernie am meisten betroffen, wobei auch der andere Lungenflügel in seiner Entwicklung eingeschränkt wird.
Handelt es sich um eine linksseitige Zwerchfellhernie, rutscht meist der Magen in den Brustraum und drückt das Herz etwas weiter in die Mitte.
Handelt es sich hingegen um eine rechtsseitige Zwerchfellhernie, rutscht die Leber und manchmal auch der Darm in den Brustraum und verlagern das Herz nach links.

## Welche Ursachen haben angeborene Zwerchfellhernien?
Die genauen Ursachen für angeborene Zwerchfellhernien sind nicht bekannt. In etwa 20% der Fälle handelt es sich um eine Begleiterscheinung genetischer Defekte, die auch weitreichende andere Komplikationen mit sich bringen können.
Darum sollte eine Untersuchung durchgeführt werden, ob ein genetischer Defekt vorliegt.
Dazu muss allerdings eine Fruchtwasserpunktion gemacht werden, die für die Mutter unangenehm ist und in seltenen Fällen eine Fehlgeburt auslösen kann.

In den restlichen 80% ist die Ursache noch immer nicht bekannt. 
Es kann ein Unfall oder eine Krankheit in der achten bis zehnten Schwangerschaftswoche ausgelöst haben, vielleicht aber auch nur ein Schluckauf der Mutter, eine Erschütterung oder bestimmte Chemikalien aus dem Grundwasser.
Tatsache ist, dass man eine angeborene Zwerchfellhernie nach aktuellem Kenntnisstand nicht absichtlich herbeiführen kann und die Eltern niemals die Verantwortung dafür tragen.

## Welche Behandlungsmöglichkeiten gibt es?
Eine angeborene Zwerchfellhernie kann nach der Geburt operativ korrigiert werden. 
Die in den Brustraum gerutschten Organe werden in den Bauchraum zurück geschoben und das Loch im Zwerchfell mit einem kegelförmigen Patch geschlossen.
Manchmal ist der Bauchraum zu klein gewachsen, um die Organe vollständig hinein zu schieben.
Das ist kein Problem, dann wird zunächst nur ein Teil der Organe in den Bauch geschoben und die Bauchdecke somit gedehnt.
Nach einigen Tagen wird dann auch der Rest in den Bauchraum geschoben.
Dieser Eingriff ist nicht in allen Krankenhäusern möglich, allerdings doch in den meisten größeren Krankenhäusern wie beispielsweise der Charite und dem Vivantes Klinikum in Berlin.
Solange die Lunge noch zu klein ist, kann das Kind intubiert und mit erhöhter Sauerstoffzufuhr versorgt werden.

In einigen Fällen genügt eine Intubation allerdings nicht.
Sollte die Lunge nach der Geburt so klein sein, dass sie den Körper auch bei reiner Sauerstoffzufuhr nicht mit genügend Sauerstoff versorgen kann, gibt es noch eine letzte Rettung: extrakorporale Membranoxygenierung, kurz ECMO.
Wie der Name verrät, findet die Oxygenierung des Blutes außerhalb des Körpers statt.
Dazu wird das Blut des Patienten durch eine ECMO-Maschine geleitet, die das Blut mit Sauerstoff anreichert und Kohlendioxid entfernt.
Ungefähr 7 von 10 Kindern schaffen es, wenn sie an die ECMO-Maschine angeschlossen werden müssen.
Leider wird bei dieser Methode verschwiegen, dass ein Kind auch nicht gesund genug dafür sein kann.

Sollte es bereits absehbar sein, dass die Lunge zu klein wird, kann ein minimal-invasiver Eingriff namens "Ballonmethode" noch vor der Geburt angewand werden. 
Der korrekte Name dafür lautet "Fetoscopisch-gesteuerte Trachealokklusion (FETO)". 
Der Eingriff erfolgt normalerweise zwischen der 26. und 30. Schwangerschaftswoche.
Die Mutter wird örtlich betäubt und ein kleiner Ballon in die Luftröhre des Fötus eingeführt.
Der Ballon wird dann vorsichtig aufgeblasen, um die Luftröhre teilweise zu blockieren.
Dadurch wird der Flüssigkeitsfluss in die Lunge reduziert und der Druck in der Lunge erhöht, was dazu führt, dass sich die Lungenbläschen besser entfalten und die Lungenreifung stimuliert wird.
Der Ballon wird nach einigen Wochen oder Monaten entfernt, jedenfalls vor der Geburt.

Die Methode ist relativ neu und wird von den Ärzten nur für linksseitige Zwerchfellhernien empfohlen, da es bislang kaum Erfahrungen mit der Ballonmethode bei rechtsseitigen Zwerchfellhernien gibt.
Es gibt auch nur wenige Kliniken, die diesen Eingriff durchführen können, darunter das Universitätsklinikum Mannheim und vielleicht bald auch das Vivantes Klinikum Neukölln.
<i>
In einer Kohorten-Studie des New England journal of medicine wurde das Überleben linksseitiger Zwerchfellhernien bis zum Verlassen der Neo-Intensivstation von 15% auf 40% erhöht. 
Dabei ist anzumerken, dass nicht alle linksseitige Zwerchfellhernien eine Überlebensrate von nur 15% haben, sondern die, bei denen die Ballonmethode empfohlen aber nicht durchgeführt wird.
Aufgrund der eindeutigen Ergebisse wurde die Studie vorzeitig abgebrochen.<sup>1</sup>
</i>
<!-- https://www.uniklinikum-dresden.de/de/das-klinikum/universitaetscentren/zentrum-fuer-feto-neonatale-gesundheit/weiterbildung/journalclub-1/zusammenfassungen/praenatale-therapie-der-zwerchfellhernie.pdf -->
<!-- https://pubmed.ncbi.nlm.nih.gov/34106556/ -->

[1]: Jan A Deprest et al., "Randomized Trial of Fetal Surgery for Severe Left Diaphragmatic Hernia", The New England journal of medicine, 2021

---

# Meine Zweitgeborene
Wenn das ungeborene Kind an einer Zwerchfellhernie leidet, ist das immer auch eine große Belastung für die Eltern.
Ich musste das zusammen mit meiner Frau Linda leider am eigenen Leib erfahren.
Ende 2022 wurde meine Frau zum zweiten Mal schwanger. Im Februar 2023 - der 20. Schwangerschaftswoche - wurde bei der Feindiagnostik ein Problem festgestellt:
das Herz unseres Kindes befand sich nicht an der richtigen Stelle. Für uns und die Ärztin war die Ursache zu diesem Zeitpunkt überhaupt nicht klar.
Die einzige Gewissheit, die wir hatten, war die, dass es ein Problem gab. Wir waren am Boden zerstört, wie konnte das sein?
Wir waren jung und gesund, wieso passierte uns so etwas? 

An diesem Tag gaben wir ihr einen Namen: Enya.
Diesen Namen hatten wir schon seit einiger Zeit in unseren Köpfen, doch an diesem Tag stand er fest.
Es gibt verschiedene Deutungen dieses Namens.
Vor diesem Tag gefiel meiner Frau der Name besonders wegen der Bedeutung "kleines Feuer".
Man kann Enya auch mit "Samenkorn" übersetzen.
Doch durch diese Nachricht rückte eine andere Bedeutung in den Vordergrund: "Wasser des Lebens".

Unsere Ärztin organisierte so schnell wie möglich einen Untersuchungstermin mit präziseren Geräten 
und so saßen wir zwei Tage später bei Herrn Dr. Dietmar Schlembach in der Klinik für Geburtsmedizin des Vivantes Klinikum Neukölln.

## Die Untersuchungen
Er stellte fest, dass unser Kind - ein Mädchen - an einer rechtsseitigen Zwerchfellhernie leide. 

![Ultraschall](/zwerchfellhernie/ultraschall.png)
<small>Enya im Ultraschall</small>

Wir hatten an diesem Tag und auch an allen weiteren Terminen sehr viele Fragen und Dr. Schlembach nahm sich stets die Zeit, alle unsere Fragen zu beantworten.
Und weitere Termine gab es viele: Bei Dr. Schlembach waren wir anfangs alle vier Wochen, später alle zwei. Insgesamt waren wir bis zur Geburt sechs Mal dort, wobei es sich wie dutzende Male anfühlte.
Uns wurde erklärt, dass die Geburt nicht in unserem nächstgelegenen Krankenhaus stattfinden kann, sondern in Neukölln stattfinden muss.
Es musste nämlich nach der Geburt eine OP durchgeführt werden, bei der die in den Brustkorb gerutschte Leber in den Bauchraum zurück geschoben und das Zwerchfell verschlossen wird.
Mit etwas Pech könnten auch mehrere OPs notwendig werden.
Im schlimmsten Fall müsste die Geburt in Mannheim durchgeführt werden, weil die Ärzte dort für den absoluten Notfall die ECMO-Therapie anwenden können.
Wir könnten uns auf jeden Fall dazu entscheiden, sollte uns das lieber sein, Dr. Schlembach sah dazu allerdings keinen Anlass.
Auf jeden Fall würden aber im Laufe von Enyas Leben viele Nachuntersuchungen stattfinden und vielleicht auch noch Jahre später Korrekturoperationen notwendig werden.

In Neukölln machten wir Bekanntschaft mit einer liebevollen Seele. 
Mareen ist Arzthelferin in Neukölln und hat uns die gesamte Reise über begleitet.
An jedem Termin war sie für uns da und stets hatte sie ein Lächeln auf den Lippen.
Wir haben uns viel mit ihr unterhalten und konnten immer auf sie zählen.
Zu Ostern bastelte unsere erste Tochter Frieda eine Karte für sie und Mareen versprach uns, uns und Enya auch nach der Geburt hier im Krankenhaus besuchen zu kommen.

Zudem waren wir noch bei Frau Dr. Hampel zur genetischen Untersuchung. Sie war eine der vielen unglaublich freundlichen und hilfsbereiten Personen, denen wir auf dieser Reise begegnen sollten.
Zuvor wurde bei Linda in Neukölln eine Fruchtwasserpunktion durchgeführt, um genetisches Material von Enya untersuchen zu können.
Ziel war es, eine mögliche Trisomie feststellen zu können.
Leider ist dieser Eingriff sowohl schmerzhaft als auch riskant, da durch etwa 2% aller Fruchtwasserpunktionen eine Fehlgeburt ausgelöst wird.
Die Entnahme war allerdings erfolgreich, sodass wir uns zwei Wochen später zur Auswertung in Dr. Hampels Humangenetik aufmachten.

Wir waren längst in Berlin, als uns eine Helferin von Dr. Hampel anrief, dass der Termin heute leider abgesagt werden müsse, weil Frau Doktor bei ihrem erkrankten Kind zu Hause bleiben müsse. 
Wir hatten bereits zwei Wochen lang angsterfüllt gewartet und ich hatte die Tage zuvor sehr viel gearbeitet, um mir an diesem Tag frei nehmen zu können, darum war unsere Enttäuschung groß.
Die Arzthelferin kontaktierte ihre Chefin und diese veranlasste, dass wir ihre private Telefonnummer bekamen. 
Wir setzten uns in ein Café, fassten allen Mut zusammen und riefen die Nummer an.

Endlich gab es einen Lichtschein: Unsere kleine Enya war genetisch einwandfrei. Das hatte zumindest die schnelle Untersuchung ergeben.
Als wir eine Woche später erneut nach Berlin zu Frau Dr. Hampel fuhren, wurde uns wie vermutet verkündet, dass auch die zweite Untersuchung keine genetischen Defekte feststellen konnte.
Das war wunderbar, denn eine genetische Krankheit hätte noch viele weitere Probleme mit sich bringen können. So wussten wir, dass wir es "nur" mit der Zwerchfellhernie zu tun hatten.

## Magnet-Resonanz-Tomographie
Als wäre das alles noch nicht genug gewesen, musste auch noch ein MRT gemacht werden.
Für dieses MRT fuhren wir nach Berlin Buch, eine ziemlich weite Reise, vor allem, weil ich zu diesem Zeitpunkt noch keinen Führerschein hatte.
Mein Vater aber arbeitete zu diesem Zeitpunkt in diesem Klinikum (in der IT und dem Betriebsrat) und konnte uns darum mitnehmen.
Erst später erfuhren wir, dass er eigentlich an diesem Tag krankgeschrieben war.
Und trotzdem fuhr er uns den weiten Weg nach Buch und holte uns auch wieder ab.
Es ist diese Art der Liebe und Unterstützung, die eine junge Familie nicht an einer Katastrophe zerbrechen lässt.

In Buch erfuhren wir, nachdem wir lange bei der Anmeldung der Radiologie anstanden, dass wir zunächst in eine andere Etage mussten, um meine Frau dort als Patientin anzumelden.
Bei der Anmeldung mussten wir erneut sehr lange warten und stellten fest, dass wir den Mutterpass vergessen hatten.
Eigentlich war dies eine Katastrophe, aber die Schwester sah darüber hinweg und fragte uns einfach nach den nötigen Informationen.
Wieder bei der Radiologie warteten wir erneut sehr lange, bis meine Frau aufgerufen wurde.
20 bis 30 Minuten sollte das ganze dauern, aber erst nach 1,5 Stunden kam sie wieder heraus.
Sie schilderte mir eine der unangenehmsten und beängstigstenden Erfahrungen, die sie jemals erleben musste.

Die Röhre war derart eng und laut, dass es sehr schrecklich gewesen sein musste.
Glücklicherweise konnte man ihr zur Beruhigung während der Messung Musik anmachen und meine Frau entschied sich für die entspannende Musik von der Sängerin Enya.
Sie entschied sich für diese Musik, um unserem Kind besonders nah zu sein.
Es hatte sich im Laufe der Wochen ergeben, dass uns diese Musik Trost und Nähe schenkte.
Als sie heraus kam, war sie einfach nur froh dieses MRT hinter sich zu haben und wollte nie wieder in so eine Röhre.

![MRT](/zwerchfellhernie/mrt.png)
<small>Magnet-Resonanz-Tomographie</small>

Die Auswertung kam wenige Wochen später.
Man wollte mit dem MRT das Volumen von Enyas Lunge messen, aber die Bilder waren nicht aussagekräftig und es wurde ein zweites MRT angeordnet, dieses Mal im MRT im Klinikum Neukölln.
Warum dieses Mal in Neukölln und warum wir beim letzten Mal nicht schon dort waren, war uns nicht bekannt.
Es war wahrhaft schrecklich für meine Frau, erneut in diese Röhre und die lauten Geräusche hören zu müssen, aber sie tat es.
Sie tat es für unser Kind, für unsere kleine Enya.
Schon die Liebe, die ein Vater für sein Kind empfindet, ist schwer zu beschreiben, aber die Liebe einer Mutter geht noch weit darüber hinaus.
Sie ist bedingungslos und aufopfernd, kräftigend und zugleich zärtlich.
In ihr wohnt eine Vollkommenheit inne, die als lebenslanger Begleiter die einzigartige Bindung zwischen Mutter und Kind repräsentiert und der Ausdruck reinster Weiblichkeit ist.

Und aus diesem Grund ging meine Frau wieder in so eine fürchterliche, beengende Röhre, diesmal direkt in Neukölln.
Um dorthin zu gelangen, konnten wir die unterirdischen Gänge des Klinikenkomplexes nutzen, was zugegebenermaßen spannend war und unserer älteren Tochter gut gefiel.
Dieses Mal hatte meine Frau keine Möglichkeit, während des MRTs Musik zu hören, aber sie konnte, wenn sie nach oben sah, ein klein wenig aus der Röhre schauen, was ihr half.
Die Auswertung kam wieder wenige Wochen später und bestätigte nur, was bislang ohnehin vermutet wurde, dass die Lunge unseres Kindes stark genug war, um in Neukölln geboren zu werden.

Kurze Zeit später wurden wir nach einer Untersuchung bei einer Vertretung von Herrn Dr. Schlembach gebeten, noch kurz zu warten, weil der Neonatologe noch einmal mit uns sprechen wollte. Leider gab es einen Notfall und wir mussten einige Stunden warten.
Gegen 18 Uhr wollten wir schon gehen, weil wir sonst nicht mehr mit den öffentlichen Verkehrsmitteln nach Hause gekommen wären.
Da wurden wir noch einmal zu der Vertretungsärztin gebeten.
Ihr wurde zuvor nicht mitgeteilt, dass einige Tage vorher eine erneute Begutachtung der MRT Bilder von den Ärzten und Radiologen stattfand.
In dieser Begutachtung änderte sich die Einschätzung der Experten.
Das Risiko bei einer Geburt in Neukölln sei doch unangenehm hoch und uns wurde geraten, nach Mannheim zu gehen.
Die Entscheidung lag dennoch bei uns.

Diese neue Wendung sollte alle unsere Pläne auf den Kopf stellen.
Wir hatten uns so sehr auf Neukölln eingestellt und vorbereitet.
Meine Tante hat eine Wohnung in Berlin, zu der hatten wir uns die Schlüssel geholt.
Wir waren den Weg von der Wohnung zum Vivantes Krankenhaus abgefahren.
Wir hatten Listen erstellt, was ich mit Frieda alles in Berlin unternehmen könnte.
Wir mussten uns ja genauso Gedanken machen, wie wir diese schwierige Zeit am besten für unsere erste Tochter gestalten konnten.
Das alles war mit einem Mal zunichte gemacht.

Und wir sollten dort vollkommen alleine sein, hunderte Kilometer von unseren Familien und Freunden entfernt.
Mit niemandem bei uns, der uns unterstützen könnte.
Vielleicht müssten wir sogar Frieda zurück lassen.
Aber sie war zu diesem Zeitpunkt nur 2,5 Jahre alt, wir hätten sie nicht Wochen oder Monate bei ihren Großeltern lassen können.
Das ganze jagte uns eine riesen Angst ein.
Es war eine von vielen Situationen, in denen wir uns ohnmächtig und ohne Kontrolle gefühlt hatten.

## Vorbereitungen auf Mannheim
Die MRT-Bilder wurden auch an Prof. Dr. Thomas Schaible geschickt, dem Direktor der Klinik für Neonatologie des Uniklinikums Mannheim.
Das Universtätsklinikum Mannheim ist mittlerweile zu dem Zwerchfellhernien-Zentrum geworden, das europaweit die meisten Neugeborenen pro Jahr behandelt.
Diese Ärzte und Schwestern sind durch ihre hochentwickelte Technologie und den täglichen Umgang mit Zwerchfellhernien die Besten der Besten, wenn es um Zwerchfellhernien geht. 
Und dieser Prof. Dr. Schaible rief mich nun eines Tages auf meinem Handy an.
Er hatte die MRT Bilder bekommen und schätzte, dass wir in Neukölln nur eine Überlebenschance von etwa 30% hätten.
Sollten wir nach Mannheim kommen, könnten sie die Überlebenschancen für unsere Enya auf etwa 70%-80% erhöhen.

Das waren nicht die Zahlen, auf die wir gehofft hatten, aber es war uns genug.
Dann fahren wir eben nach Mannheim, was auch immer getan werden musste, es wird getan.
Also fuhren wir am 31. Mai die knapp 700 Kilometer nach Mannheim, um am nächsten Tag in Mannheim die Geburtsplanung durchzuführen.
Unsere Tochter ließen wir bei meinen Eltern.
In der Nähe des Klinikums gab es ein Elternhaus, in dem Eltern von betroffenen Kindern kostengünstig unterkommen können.
Leider waren die Zimmer meist besetzt, doch für diese eine Nacht konnte uns die freundliche Besitzerin eine hübsche Wohnung mit Küche, Bad und Balkon geben.
Der Balkon wurde mit der Nachbarwohnung geteilt und so lernten wir Linda kennen.
Das freute uns, denn der Name meiner Frau ist ja ebenfalls Linda.

Linda hatte ihr Kind bereits Monate zuvor als Frühchen bekommen, sie war damals gerade erst in der 25. Woche.
Wir unterhielten uns ein wenig und erfuhren, dass auch ihr Kind von Prof. Dr. Schaible behandelt wird.
Seit Monaten lebte sie mit ihrem Mann in diesem Elternhotel und täglich pendelten sie zu dem etwa einen Kilometer entfernt liegenden Krankenhaus, um bei ihrer Tochter Rosalie sein zu können.
Ich hatte seit einiger Zeit angefangen, täglich für Enya zu beten, doch von diesem Tag an betete ich für Enya und für Rosalie.

Als wir dann am 01.06. zu der Geburtsvorbereitung in das Klinikum Mannheim kamen, waren wir zunächst von diesem wunderschönen Universitätsklinikum überwältigt.
In der großen Parkanlage gab es einen Teich mit Schildkröten und Enten darin und in den Bäumen lebten Papageien.
Zugegebenermaßen wussten wir nicht wirklich, wo wir hinmussten, aber nach einigem hin und her fanden wir die richtige Station.
Leider hatte Herr Schaible unsere Verabredung nicht an die Verwaltung weiter gegeben, so kam es, dass wir gar keinen Termin hatten.
Das sorgte zunächst für Verwirrung, konnte mit einem Anruf bei Prof. Dr. Schaible allerdings geklärt werden.

Nachdem wir uns angemeldet hatten, holte Dr. Schaible uns ab und wir gingen auf die Neo-Intensivstation, die 30/4.
Dort angekommen redeten wir zunächst miteinander.
Er erklärte uns noch einmal genau, was eine Zwerchfellhernie ist und wie sie behandelt wird.
Er erklärte uns, wie die ECMO-Therapie funktioniert und welche Risiken sie birgt.
Wir sahen uns auch gemeinsam die Aufnahmen von Enya an und er wies uns darauf hin, dass sie für ihr Alter schon besonders viele Gehirnwindungen hatte, was auf eine hohe Intelligenz hinweisen kann.
Zuletzt redeten wir noch über die Art der Geburt.
Während eine natürliche Geburt für das Kind überhaupt kein Problem darstellt, riet er uns doch ausdrücklich zum Kaiserschnitt, aus organisatorischen Gründen.
Mit einem geplanten Kaiserschnitttermin war sichergestellt, dass alle notwendigen Ärzte anwesend, ausgeschlafen und nicht anderweitig beschäftigt sind.
Meiner Frau gefiel das gar nicht, da sie stets eine natürliche Geburt vorzog und Angst vor einem Kaiserschnitt hatte.
Mir ging es damit ähnlich, doch mussten wir beide eingestehen, dass ein Kaiserschnitt für die Behandlung unserer Tochter am Besten war.

![Prof. Dr. Schaible](/zwerchfellhernie/umm_Neonatologie_Schaible.jpg)
<small>Prof. Dr. Schaible [©Uniklinik Mannheim](https://www.umm.de/klinik-fuer-neonatologie/mitarbeiterinnen/ "Mitarbeiter:innen: Uniklinik Mannheim")</small>

Anschließend durften wir noch ein ECMO-Gerät in Betrieb sehen.
Es war merkwürdig, dieses kleine, zarte Wesen dort schlafend auf dem Bett in der Mitte des Raums liegen zu sehen, viele Geräte drum herum und Schläuche und Kabel am ganzen Körper.
An diesem Tag nahm ich eine weitere Seele in meine Gebete auf: Leonie.

Als alle Fragen geklärt waren, gingen wir noch zur Geburtsvorbereitung, bei der der Kaiserschnitt mit uns geplant wurde und meine Frau einige erschreckende Sachen unterschreiben musste.
Der Termin wurde auf den 23.06. gelegt, 15 Tage vor dem berechneten Entbindungstermin.
Anschließend machten wir uns wieder auf den weiten, weiten Heimweg.

## Mannheim
Eines Montags, es war am 12.06, 1 1/2 Wochen vor dem Kaiserschnitttermin, hatte Linda am Nachmittag eine Routineuntersuchung in Neukölln.
Ich litt zu dieser Zeit an schlimmen Fieber, darum blieb ich zu Hause.
Glücklicherweise konnte Lindas Mutter Bettina sie und Frieda nach Neukölln fahren.
Schon dort war sie eine wundervolle Unterstützung.
An diesem Tag stellte sich heraus, dass der Druck auf der Fruchtblase viel zu hoch war und die Fruchtblase jederzeit platzen könnte.
Da Enya unbedingt in Mannheim zur Welt kommen musste, wäre dies schrecklich gewesen.
Schon seit Wochen hatte sie sich zwei weiche Matratzen übereinander gelegt, da sie sonst vor Rippenschmerzen kein Auge zu bekam.
Die Ursache war, dass Enya durch ihre Zwerchfellhernie nur sehr viel weniger Fruchtwasser schlucken konnte als im Normalfall.

Der Anruf meiner Frau mit dieser Neuigkeit erwischte mich eiskalt.
Sie hatte in ihrer typischen Art bereits eine Packliste geschrieben und in meinem Fieberwahn versuchte ich, alles von der Liste zusammen zu packen.
Leider benötigte ich teilweise minutenlang, um etwas einfaches wie eine Zahnbürste zu finden.
An diesem Abend erreichten wir leider keinen Krankentransport mehr, doch schon am nächsten Morgen konnte ich einen Krankentransport nach Mannheim organisieren.

Voller Aufregung und Panik packten wir die letzten Sachen für meine Frau.
Sie weinte dabei die ganze Zeit.
Das alles wurde ihr zu viel.
Nun sollte sie ganz alleine nach Mannheim fahren und hatte dort keine Unterstützung.
Insbesondere die Trennung von Frieda fiel ihr besonders schwer und sie wusste nicht, wann wir nachkommen könnten.

Nach knapp zwei Stunden stand der Transport vor unserer Tür und eine nette Frau begrüßte uns.
Sie trug den Spitznamen "Sternchen", da sie viele Sternentatoos trug.
Sie ließ uns in aller Ruhe voneinander verabschieden.
Es war ein herzzerreißender Abschied.
Ich wollte so schnell wie möglich nachkommen, aber unsere Sachen für einen Aufenthalt von mindestens einem Monat waren noch nicht fertig gepackt und Lindas Mutter war selbstverständlich auch nicht auf einen derart kurzfristigen Aufbruch vorbereitet.
Sie hatte nämlich vor, uns die erste Woche nach Mannheim zu begleiten, um uns bei zu stehen und sich um unsere ältere Tochter zu kümmern.

Meine Frau fing Mittwoch Morgen an, einigen Leuten privat von der aktuellen Situation zu schreiben.
Sie fühlte sich einsam und allein und ein enormer Druck lastete auf ihr.
Das Elternhotel war voll und in dieser Woche stand in Mannheim die Bundesgartenschau an, darum war bereits seit langem jedes Hotelzimmer der Stadt ausgebucht.
Sie wusste also nicht, wann ihr Mann, ihre Tochter und ihre Mutter nachkommen könnten.
Da sich sehr viele Leute an unserer Geschichte interessiert zeigten, eröffnete meine Frau noch an diesem Morgen einen Telegram Kanal, damit sie nicht jede Neuigkeit an 30 Leute einzeln schreiben musste.
Sie musste sich vielen anstrengenden Untersuchungen hingeben, hatte allerdings ansonsten viel Ruhe.
Sie genoss die Zeit ohne Stress, eine Zeit nur für sich und Enya.
Sie bekam in dieser Zeit durch Freunde und Familie viel seelische Unterstützung und nahm die Zeit insgesamt positiv wahr.

Zu dieser Zeit war es sehr heiß und Linda ging besonders morgens gerne in den Park des Klinikums.
Dort lernte sie eine andere junge Mutter namens Julia kennen.
Auch sie war hoch schwanger und erwartete ein Mädchen mit einer Zwerchfellhernie.
Und auch für sie war dies ihr zweites Kind.
Der einzige Unterschied zu unserem Fall war, dass ihr Kind eine der häufiger auftretenden linksseitigen Zwerchfellhernien hatte, während bei unserem Kind eine der seltenen rechtsseitigen Hernien vorlag.

![Der Klinik-Park in Mannheim](/zwerchfellhernie/klinikpark.png)
<small>Der Klinik-Park in Mannheim</small>

In der Nacht von Mittwoch zu Donnerstag hatte Linda einen sehr bewegenden Traum.
Sie träumte, dass sie Enya an diesem Donnerstag zur Welt bringen würde und sie nicht sofort an die ECMO konnte, weil das Mädchen von Julia zur gleichen Zeit geboren wurde und zuerst dran musste.
Linda hatte in ihrem Traum einfach solange mit Enya gekuschelt, sie geküsst und mit ihr gespielt.
Enya war dort schon viel weiter entwickelt als ein gewöhnliches Neugeborenes und hatte viele blonde Haare auf dem Kopf.
Die Geburtszeit war in ihrem Traum allerdings ungewiss, da sie auf viele verschiedene Uhren blickte, aber alle ganz unterschiedliche Zeiten angezeigt hatten.

Meiner Frau bescherte dieser Traum viel Liebe und Hoffnung, doch noch an diesem Morgen hatte sie einen Blasenriss.
Erst dann entschieden wir uns für ihren vollständigen Namen: Enya Luise Ferun Zenz.
Plötzlich setzten die Wehen ein und meine Frau bekam einen Wehenhemmer.
Der Kaiserschnitttermin war auf Freitag Morgen, den 16.06., angesetzt und sollte möglichst eingehalten werden.
Prof. Dr. Schaible war zu diesem Zeitpunkt leider in Hamburg bei einem Kongress.
Ich hatte noch immer keine Unterkunft in Mannheim gefunden und das brachte mich zur Verzweiflung.
Ich befand mich mittlerweile in einem Zustand chronischer Panik.

Unsere liebe Freundin Suse interpretierte Lindas Traum leider ganz anders.
Sie war zufälligerweise an diesem Morgen in der Nähe von Mannheim auf dem Weg zu einem Seminar.
Sie machte einen Zwischenhalt in Mannheim und schenkte meiner Frau Gesellschaft, als sie es am meisten brauchte.
Als die Wehen zum Abend hin vollständig verschwanden, schickte Linda Suse zu ihrem Seminar.

Erst am Donnerstag Nachmittag kam ich mit Frieda und Bettina los.
Das war schon ziemlich spät, denn der Kaiserschnitt war ja auf Freitag Morgen angesetzt und wir hatten noch eine lange Fahrt vor uns.
Mein gesundheitlicher Zustand hatte sich bis dahin kaum gebessert, zumal ich sehr unter Stress stand und vieles zu erledigen hatte.
Dazu kam noch, dass ich Frieda emotional auffangen musste, eine große Herausforderung in dieser schweren Zeit.
Das Elternhotel hatte leider noch immer kein Zimmer und keine Wohnung verfügbar.
Darum mietete unsere enge Freundin Laura zwei Unterkünfte über airbnb für uns, um mich zu entlasten.
Sie hatte uns zu dieser schweren Zeit in vieler Hinsicht sehr unterstützt.

Wir, das sind ich, meine Tochter und meine Schwiegermutter Bettina, kamen gegen Mitternacht in unserer Unterkunft in Mannheim an.
Wir gingen ins Bett und standen schon wenige Stunden später wieder auf.
Ein kurzes Frühstück und auf ging es ins Klinikum.

Meiner Frau ging es zu diesem Zeitpunkt derart schlecht, dass sie uns nicht einmal die Zimmertür öffnen konnte.
Sie hatte seit einer ganzen Weile nichts essen und nur sehr, sehr wenig trinken dürfen.
In Kombination mit den hohen Temperaturen fühlte sie sich völlig ausgetrocknet und erschöpft.
Wir waren unendlich froh, uns endlich wieder zu sehen.

Lindas Traum bewahrheite sich und bei Julia gingen die Wehen los. 
Ihr Kind kam an diesem Morgen sehr schnell, weshalb die Ärzte sich zunächst um ihr Kind kümmern mussten.
Unser Kaiserschnitttermin wurde aus diesem Grund von 9 Uhr auf 11-12 Uhr verschoben.
Wir wurden gegen 11 Uhr gebeten, uns zur Entbindungsstation zu begeben.
Meine Frau wurde auf den Kaiserschnitt vorbereitet und wir warteten.
Erst gegen 14 Uhr ging der Kaiserschnitt endlich los.
Meine Frau wurde in den OP Saal gebracht, um die PDA zu bekommen.
Ich musste mich umziehen und draußen warten, da ich nur für den Eingriff herein durfte.

## Die Geburt
Ich hörte einen markerschütternden Schrei von meiner Frau.
Wie ich später von ihr erfuhr, bekam sie während der PDA eine Panikattacke.
Sie konnte nicht still halten und der Anästhesyst musste mehrmals zustechen.
Und ich saß draußen im Flur, wartete eine unerträgliche Ewigkeit und konnte nichts für sie tun.

Endlich wurde ich in den OP-Saal geholt.
Um meine Frau standen etliche Ärzte, meine Frau lag auf dem Rücken, die Arme nach links und rechts auf Ablagen ausgestreckt, als würde sie auf ein Kreuz genagelt werden.
Ihr Gesicht war tränenüberströmt und ihre panischen Augen suchten die meinen.
Ich setzte mich auf einen Hocker hinter ihrem Kopf und griff eine ihrer Hände.
Neben mir saß der Anästhesyst und vor uns hing ein Vorhang, sodass wir den Bauch meiner Frau nicht sehen konnten.

Die Operation begann.
Meine Frau redete schwach und voller Angst zu mir, ich versuchte sie zu beruhigen und Halt zu geben, so gut ich nur konnte.
Ein paar Mal übergab sie sich.
Nach sehr kurzer Zeit, es konnten nur zwei Minuten gewesen sein, wurde der Vorhang aufgezogen und wir sahen um 14:42 Uhr unsere kleine Enya.
Sie gab ein leises Geräusch von sich, ein kleines Weinen, dann sollte ihr die Nabelschnur durchgeschnitten werden.
Wir hatten zuvor den Wunsch geäußert, dass die Nabelschnur so lange wie möglich intakt bleiben sollte und diese kurze Zeit war mir definitiv zu kurz!
Ich rief: "Nein, warten Sie!" und streckte die Hand aus und der gesamte Operationssaal schrie auf.
Ich hatte meine Hände zwar gewaschen und desinfiziert, aber im Operationsbereich hatten sie trotzdem nichts zu suchen.
Sie respektierten unseren Wunsch und warteten noch einige Sekunden, meinten dann aber, dass das Kind nun dringend zu den Neonatologen müsse.

Aus diesem Grund wurde die Nabelschnur getrennt und der Vorhang wieder geschlossen.
Enya wurde in den Nebenraum gebracht, wo sie behandelt wurde, während meine Frau wieder verschlossen wurde.
Meine Frau war sichtlich erleichtert und wir konnten sogar scherzen, dass das, was sie da mit ihr anstellten, so klang, als würde man sie mit Bauschaum auffüllen.

Nach einiger Zeit wurde ich in den Nebenraum gerufen.
Endlich durfte ich meine Tochter in Ruhe betrachten.
Mir fiel sofort auf, dass sie überraschend groß war.
Etwas später erfuhr ich, dass sie 53cm groß war und 3950g wog.
Das war eine ziemliche Überraschung, da ihr eigentlicher Geburtstermin erst drei Wochen später war und sie somit gerade so als Frühchen zählte.

![Enya nach ihrer Geburt](/zwerchfellhernie/enya1.png)
<small>Enya nach ihrer Geburt</small>

Sie lag nackt auf einem weichen Tisch und schlief.
In der Nase hatte sie zwei Schläuche; an der linken Hand ein Tropf, am Fuß ein Messgerät.
Über ihr war eine Plexiglasplatte, aber von der Seite konnte man sie berühren.
Auf ihrem kleinen Körper waren noch von der Geburt Blut und Fruchtschmiere.
Ich betrachtete sie eine Zeit lang und durfte sie auch berühren.
Ich legte meine Finger auf ihren Arm, da man mir gesagt hatte, dass es für Leute im künstlichen Schlaf sehr unangenehm sein kann, gestreichelt zu werden.
Ich genoß einfach den Moment, endlich meine Tochter anfassen zu können.
Abschließend machte ich auch noch ein paar Fotos von ihr für meine Frau.

Prof. Dr. Schaible war an diesem Tag auf einem Kongress in Hamburg, aus diesem Grund war der leitende Neonatologe der Oberarzt Dr. Bayer.
Nachdem ich mir in Ruhe etwas Zeit für Enya genommen hatte, nahm mich Dr. Bayer zur Seite.
Er erklärte mir, dass Enyas Blutwerte schlechter waren, als man angenommen hatte, und dass sich in den nächsten 1-2 Stunden entscheiden würde, wie es weiter ginge.

Ich kam zurück zu meiner Frau und wir wurden wieder in den Raum gebracht, in dem wir auch schon auf den Kaiserschnitt gewartet hatten.
Linda versuchte sich an verschiedenem Essen und Trinken, erbrach allerdings alles wieder.
Ich erklärte ihr, was mir der Arzt gesagt hatte, und so saßen wir dort und warteten.

# Schicksal
Wir warteten eine ganze Weile, ohne etwas über Enya zu hören.
Gelegentlich kam eine Schwester herein und kümmerte sich um meine Frau, aber sie wussten nie etwas über unser Kind.
Auf unsere mehrfache Nachfrage sagte man uns, man würde mal nachfragen, allerdings kam nie eine Antwort.
Wir versuchten uns die Zeit möglichst angenehm zu vertreiben.
Wir malten uns eine gemeinsame Zeit aus.
Was würden wir alles tun, wenn dies alles überstanden war?

Nach zwei Stunden meldete sich endlich jemand bei uns.
Ich dürfe zu Enya, meine Frau aber müsse zunächst auf ihr Zimmer und sich ausruhen.
Ich bekam die Anweisung, zur Neo-Intensivstation 30/4 zu gehen.
Linda lag allerdings noch mindestens eine halbe Stunde alleine da, die ihr unendlich lang und qualvoll vorkam, bevor jemand kam und sie in ein neues Zimmer brachte.

Auf der Neo-Intensivstation angekommen musste ich klingeln und wurde herein gelassen.
Meine kleine Enya lag in der Mitte eines Raumes, hinter ihr eine Wand von Geräten, an denen sie angeschlossen war.
Zu dem Tropf an der Hand, den Schläuchen in der Nase und dem Messgerät am Fuß waren nun auch noch Messelektroden unter ihren Schlüsselbeinen und an ihrer Seite gekommen.
Mittlerweile hatte man ihr eine Windel umgemacht.
Die Schwester erklärte mir, wie ich mit Enya umgehen sollte und worauf ich zu achten hatte.
Dann ließ man mir ihr Bett herab, damit ich nicht stehen musste, sondern mich zu ihr setzen konnte.

Ich betrachtete sie aufmerksam von allen Seiten.
Ihre Haare waren schon schön lang und sie war etwas sauberer gemacht worden.
Ich schickte meiner Frau gerade ein Foto von ihr, als Dr. Bayer herein kam.
Meine Frau fragte mich, ob unser Kind an der ECMO hinge, was ich verneinte, bevor ich mich dem Arzt zuwandte.
Der Arzt kam direkt zum Punkt.
Er erklärte mir, dass ihre Lunge noch kleiner sei, als man erwartet hatte.
Dafür sprachen die Röntgenaufnahmen sowie die Blutwerte und die ausgeatmete Luft.
Aus diesem Grund konnte er mir leider nur die schlechte Nachricht überbringen, dass Enya es nicht schaffen werde.

Ich fing an zu weinen.
Bis dahin hatte ich den Gedanken dieser Möglichkeit nicht zugelassen.
Natürlich hatte ich mir Sorgen gemacht, doch erst jetzt konnte ich es glauben.
Und auf einmal brach alles zusammen.
Ich konnte nichts mehr für sie tun.
Monatelang hatte ich jeden Tag für sie gekämpft, keine Kosten und Mühen waren zu hoch für sie gewesen.
Wir hatten alles versucht, medizinisch wie spirituell.
Wir hatten wirklich alles gegeben, aber Enya sollte nicht bei uns bleiben.
Ich sollte meine Tochter sterben sehen.

Ich kannte meine Verantwortung und begab mich in einen Zustand der Emotionslosigkeit.
Ich fing an, nur noch zu funktionieren und das klappte ganz gut, wenn auch nicht immer.
Dieses Abschalten der Emotionen behielt ich lange bei und es war ein großer Fehler, auch wenn es vielleicht nötig war.
Aber noch Monate später hatte ich Schwierigkeiten zu erkennen, was ich überhaupt empfand.

Ich fragte, wie lange sie aushalten könne und Dr. Bayer antwortete, dass ein paar Stunden, vielleicht auch ein Tag möglich sind.
Weiterhin erklärte er, dass sie gerade reinen Sauerstoff sowie ein Gas bekomme, welches ihre Lungenbläschen vergrößere, damit sie mehr Sauerstoff aufnehmen kann.
Ihr linker Lungenflügel war etwa 4-5ml groß, der rechte quasi nicht vorhanden.
Insgesamt kam sie auf ein Lungenvolumen von 20-25% dessen, was normal gewesen wäre.
Früher kamen auch solche Kinder an die ECMO, dagegen sprach medizinisch nichts.
Heutzutage wussten wir allerdings, dass ab gewissen Grenzwerten in den Blut- und Atemwerten die ECMO das Unvermeidliche nur herauszögerte, sich aber keine Besserung einstellen würde.
Wir hatten entweder die Möglichkeit, zu warten wie lange sie aushalten würde, oder ihre Beatmung entfernen zu lassen und sie ein einziges Mal ohne Schläuche auf den Armen halten zu können.
Sobald die Schläuche entfernt wären, würde sie allerdings nur noch wenige Minuten zu leben haben.

Der Arzt ließ mich mit Enya alleine und ich gab mich meiner Trauer und Verzweiflung hin.
Als ich wieder auf das Telefon sah, bemerkte ich, dass mich meine Frau mehrfach angeschrieben und angerufen hatte und auch Bettina hatte versucht, mich zu erreichen.
Dass ich nur gesagt hatte, dass Enya nicht an der ECMO hinge und mich danach nicht weiter gemeldet hatte, hatte meine Frau berechtigterweise stark beunruhigt und sie stand kurz vor einem nervlichen Zusammenbruch.

Wie sollte ich meiner Frau die grausamen Neuigkeiten mitteilen?
Ich konnte es nicht.
Sie sollte es nicht so erfahren, ich musste bei ihr sein, sie in den Arm nehmen können.
Ich schrieb ihr, sie solle zu mir kommen, ich könne nicht mehr sagen und ich bat den Arzt darum, dass er mit Lindas Ärzten reden solle, damit sie sie schon zu uns ließen.
Dies war erfolgreich und schon bald wurde meine Frau im Rollstuhl zu uns geschoben.
Wir warteten kurz auf den Arzt und er erklärte Linda alles.
Sie schrie Dr. Bayer an. Ihr Herz schrie, während ihr Verstand realisierte.
Sie konnte kaum klar denken und rief immer wieder "Nein!"
Sie warf ihm vor, er würde sich irren.
Es konnte einfach nicht wahr sein, nach allem, was wir durchgestanden hatten.
Gleichzeitig hatte sie Höllenqualen vom Kaiserschnitt.
Die Schmerzmittel halfen nicht.
Ich versuchte, für meine Frau da zu sein, aber was konnte ich schon tun, als sie zu halten und mit ihr zu weinen?

# Unser Leben als vierköpfige Familie
Nach kurzer Zeit holte ich meine Schwiegermutter Bettina und unsere ältere Tochter Frieda, die zu diesem Zeitpunkt zwei Jahre alt war.
Während ich weg war, sang Linda für Enya Kinderlieder und sprach mit ihr.
Als ich Bettina fand, war sie guter Dinge und sehr zuversichtlich.
Sie wollte schnell zu ihrer Tochter und ihrer Enkelin.
Ich hielt sie auf und erklärte, dass Enya keine Chance hätte.
Das traf sie vollkommen unerwartet und sie reagierte fassungslos.
Diese Nachricht erinnerte sie viel zu schmerzhaft daran, wie 25 Jahre zuvor ihr eigener Sohn im Alter von 6 Jahren starb.
Es muss schrecklich gewesen sein, dieses Schockmoment erneut erleben zu müssen.

Ich brachte sie zu meiner Frau, wobei ich vor der Tür der Neo-Intensivstation mit meiner Tochter stehen bleiben musste, weil Kinder dort nicht hinein durften.
Nach einiger Zeit kam allerdings eine Schwester und holte uns doch.

![Enya auf dem Arm ihrer Mutter](/zwerchfellhernie/enya2.png)
<small>Enya auf dem Arm ihrer Mutter</small>

Als wir ankamen, saß meine Frau noch im Rollstuhl und hatte Enya auf dem Arm.
So verbrachten wir einige Zeit.
Frieda malte ein Bild, wir redeten miteinander und mit Enya und sangen ihr vor.
Eine Schwester nahm Fußabdrücke von Enya und gestaltete eine wunderschöne Karte für sie.
Zum Abend musste meine Schwiegermutter gehen, um den Schlüssel für das Apartement zu holen.
Frieda und ich kamen noch mit auf das Zimmer im Klinikum, weil Bettina von dort ein paar Sachen holen musste.
Als wir uns verabschiedeten, wollte Frieda mit ihrer Oma mitgehen.

Also ging ich alleine zurück zu meiner Frau und meinem neuen Kind.
Als ich ankam, wurde sie gerade vom Rollstuhl in ein Bett gelegt, welches man extra für sie geholt hatte.
Die ganze Aktion war gefährlich und kompliziert, weil Enya an Schläuchen und Kabeln hing, aber nach einigen Minuten hatten die Schwestern meine Frau und meine Tochter in das Bett befördert.
Lindas Schmerzen wurden schlagartig besser, als sie sich endlich hinlegen konnte.

So verbrachten wir wieder etwas Zeit.
Wir beschlossen, dass heute Abend noch Enyas Beatmung entfernt werden sollte, damit wir sie einmal ohne Schlauch sehen und uns angemessen von ihr verabschieden konnten, statt dass ihr Tod uns irgendwann überraschte.
Dann kamen Bettina und Frieda wieder zu uns.

Wir fragten eine Schwester, ob es denn wenigstens dem Kind von Julia gut ginge.
Sie durfte uns nichts sagen, machte aber eine leichte Andeutung, dass es nicht so gut aussah.
Meine Frau rief Julia an.
Wir gönnten ihr so sehr, dass wenigstens sie ihr neugeborenes Glück genießen durfte.
Aber auch ihr Kind war gestorben, eine halbe Stunde nach der Geburt, weil die Lunge einen winzigen Riss hatte, der weiter aufgerissen war.
Julia und ihr Mann kamen uns auf der Neo-Intensivstation besuchen und wir unterhielten uns ein wenig.
Sie hatten mehr als einen halben Tag lang ihre kleine Annalena im Zimmer gelassen, bis sie es psychisch nicht mehr aushielten.
Nun wirkten sie sehr stark und besonnen und wir verabschiedeten uns im gegenseitigen Mitgefühl und Beileid.

Nachdem sie gegangen waren, verabschiedeten sich auch Bettina und Frieda von Enya.
Bettina beugte sich zu ihr herunter, sprach noch ein paar Worte mit ihr und gab ihr einen Kuss.
Frieda bestand darauf, Enya den Kopf streicheln zu dürfen.

Als sie weg waren, aß ich mit meiner Frau noch schnell etwas.
Wir hatten den ganzen Tag über fast nichts gegessen und wussten, dass wir später nichts mehr essen könnten.
Eine Schwester war so lieb und holte uns Abendessen.
Obwohl es schon nach 22 Uhr war, organisierte sie uns eine Mischung aus Frühstück, Mittag und Abendbrot.

![Abschied von Enya](/zwerchfellhernie/enya3.png)
<small>Abschied von Enya</small>

Irgendwann kam der Arzt zu uns und wir sagten ihm, dass wir nun gerne die Schläuche entfernt hätten.
Es dauerte länger als ich dachte, aber nach einiger Zeit war alles von Enya entfernt, außer den Schläuchen für die Schlaf- und Schmerzmittel.
Wir machten einige Fotos und ich nahm sie das erste und einzige Mal auf den Arm.
Es war schrecklich, wie schlaff ihr Körper war, obwohl sie noch lebte.
Dann legte ich sie wieder in das Bett zu meiner Frau und legte mich dazu, sodass Enya zwischen uns lag.
Arzt und Schwestern ließen uns in Ruhe, nur als die ausgeschaltete Wärmelampe penetrant zu piepen begann, kam eine Schwester herein und schaltete das Piepen ab.
Nach einigen Minuten kam der Arzt herein, überprüfte den Herzschlag und ging wieder.
Etwa zwei Minuten später kam erneut herein.
Es war 22:54 Uhr.
Er überprüfte erneut den Herzschlag und verkündete, dass kein Herzschlag mehr zu hören sei.
Nie habe ich einen solchen Schmerz und eine solche Verzweiflung im Gesicht meiner Frau gesehen.

---

# Der Tod
In einem Film kommt an dieser Stelle eine Schwarzblende und man springt bequemerweise direkt zur Beerdigung oder "1 Jahr später".
Aber das echte Leben ist sehr viel grausamer.
Wir blieben so noch ein wenig liegen, doch nach einigen Minuten wurde Enya bleich und kühl und so wollten wir sie nicht in Erinnerung behalten, also sagten wir der Schwester, dass ich sie nun waschen wollte.
Die Schwester entfernte die letzten Schläuche und die Windel und ich wusch Enya mit zwei Lappen und einer Schale Seifenwasser.
Linda hatte ihr etwas Kollostrum auf die Brust gemacht, das war getrocknet und ich ließ es auf ihr.
Es war ein Geschenk, welches sie mit auf ihre Reise nehmen sollte.
Zudem schnitt ich einige ihrer überraschend langen Haare als Erinnerung ab.
So hatte sie etwas von uns und wir etwas von ihr.
Diese Haare sind bis heute das Einzige, was wir wirklich von ihr haben.
Dann zog ich sie mit einer roten Hose und einem rosa T-Hemd an.

Unser neues Familienmitglied zurück zu lassen, fiel uns sehr schwer.
Beim Verlassen des Raumes schauten wir darum nur uns an und sahen nicht zurück.
Wir wussten, wir würden sie nie wieder sehen, auch wenn man uns sagte, dass wir bis morgen früh jederzeit wieder zu ihr dürften oder auch die ganze Nacht bei ihr bleiben.
Wir wollten sie lebendig in Erinnerung behalten, darum wussten wir, dass wir dieses Angebot niemals annehmen könnten.
Und darum machten wir nach ihrem Tod auch keine Fotos von ihr.

Wir wurden auf unser Zimmer gebracht, ich bekam ein eigenes Bett am Fußende des Krankenbettes, in dem meine Frau lag.
Im neuen Zimmer angekommen wurde Linda noch untersucht und bekam eine Pille gegen die Milchbildung. 
Ich holte schnell die Sachen aus dem alten Zimmer meiner Frau.
Bald darauf waren wir allein.
Wir saßen etwa eine halbe Stunde einfach nur da und weinten manchmal.
Wir fühlten uns unendlich leer, glücklos und freudlos, erschlagen und erstarrt, ohne Gefühle.
Wir waren wie leere Hüllen.
Dann legten wir uns schlafen.

Die nächsten drei Tage verbrachten wir im Krankenhaus. 
Ich telefonierte mit meinen Eltern, kümmerte mich um Geburts- und Todesurkunde, um das Bestattungsunternehmen und den Totentransport.
Da wir von zu Hause fast 700 Kilometer entfernt waren, kostete uns allein der Transport von Enya nach Hause 2.000€.
Wir konnten aber in dieser Zeit auch lachen.
Wir machten uns beispielsweise jeden Tag darüber lustig, dass das Essen immer früher ins Zimmer gebracht wurde.
Als die Schwester schlussendlich um 16 Uhr mit dem Abendessen in der Tür stand, konnten wir das Lachen kaum zurück halten.
Zum Glück hatten wir noch unsere eigenen Vorräte mitgebracht.

Tagsüber kamen Bettina und Frieda zu uns.
Das war unser größter Lichtblick.
Was hätten wir nur ohne Bettina getan?
All die Tage hatte sie sich um Frieda gekümmert, war für uns da, hat viel auf sich genommen.
Sie führte viele Gespräche mit uns und baute uns auf.
Zudem bedeutete es uns sehr viel, dass es noch jemanden gab, der Enya kennen lernen durfte.
Unsere Dankbarkeit ist nicht in Worte zu fassen.
Zum Nachmittag oder Abend gingen sie wieder.
Ab dann trat bei uns wieder die bedrückende Stille ein.
Unsere Tochter litt sehr darunter, darum schlief sie die letzte Nacht bei uns.
Die Nächte zuvor war dies aufgrund von Lindas starken Schmerzen nicht möglich.

Zudem standen uns viele unserer Freunde und Familie bei.
Schon seit Linda in das Krankenhaus gefahren wurde, hatte sie regelmäßig Neuigkeiten an alle Interessierten geschrieben.
Als Antwort kamen stets Ermunterungen, Bekräftigungen und liebe Worte.
Mit der Verkündung von Enyas Tod stand man uns ebenfalls bei, mit Beileidsbekundigungen und Trauer.

Unsere wundervolle Freundin Laura hatte ein Paket für Linda zusammengestellt, das derart liebevoll gemacht und gefüllt war, dass es uns zu Tränen rührte.
Sie hatte das Paket vor Enyas Geburt abgeschickt, damals selbstverständlich mit bester Hoffnung, darum befand sich leider auch eine Babymütze darin.
Auch wenn dies natürlich unseren Schmerz förderte, waren wir dennoch unendlich gerührt.

Wir verbrachten die Tage mit Reden, Kreuzworträtseln, Spielen mit Frieda und wir gingen in den Klinik-Park.
Meine Frau war noch im Rollstuhl, darum mussten wir sie die meiste Zeit schieben.
Trotzdem war diese Zeit an der frischen Luft sehr gut für uns.
Als wir am Sonntag aus dem Park zurück ins Zimmer kamen, war Lindas Bett frisch bezogen.
Das traf sie hart, denn auf diesem Bettlaken hatte Enya gelegen.
Die Schwester, die das Bett bezogen hatte, bekam dies mit.
Sie entschuldigte sich vielmals und bot sogar an, die alte Bettwäsche zurück zu holen.
Linda lehnte dankend ab.
Diese Schwester war eine sehr liebe Person.
Sie hatte zuvor bereits Frieda zwei Spritzen zum Spielen geschenkt und gab Linda am letzten Tag besonders viele Netzunterhosen und Einlagen mit.

Jeden Vormittag kam die Frauenärztin und schaute nach Lindas Befinden.
Dabei war sie allerdings ziemlich verärgert, dass sie 24 Stunden nach dem Kaiserschnitt noch nicht alleine aufstehen konnte.
Wir fanden das ziemlich übertrieben, aber wer weiß, vielleicht wollte sie uns damit nur helfen, Motivation zu finden, um weiter zu machen.
Jedenfalls setzte sie damit Linda sehr unter Druck.
Sie wollte das alles nicht hören, sie sah keinen Sinn darin, sich zu bemühen.
Sie verstand nicht, warum die Gynäkologin weder Verständnis noch Mitgefühl zeigen konnte.
Linda hätte in dieser Zeit fast aufgegeben.
Doch mit meiner Hilfe und mit der von Bettina und Frieda schaffte sie es, sich wieder aufzurichten.

Am Montag, dem dritten Tag, wurde meine Frau entlassen. 
Wir packten alle Sachen, meldeten uns ab und ließen den Körper unserer Tochter zurück.
Linda holte die Plazenta selber aus dem Kreissaal ab.
Die lange Fahrt nach Hause wäre an diesem Tag zu viel gewesen, aber wir hatten ohnehin noch einige weitere Tage die Wohnung gebucht, darum blieben wir noch eine Nacht.
Die Wohnung war wirklich sehr schön.
Der Vermieter kam noch am Abend vorbei, um schonmal den Schlüssel ab zu holen.
Er hatte von uns zuvor erfahren, dass Linda ihr Kind bekommen sollte.
Nun stand sie dort, nicht mehr schwanger, aber auch ohne Kind, und wir befanden uns in einer dieser unangenehmen Situationen, die wir in den nächsten Monaten immer wieder erleben sollten: mit Busfahrern, entfernteren Freunden, Kollegen.
Diese unangenehmen Sekunden, wenn man ihnen unter die Augen tritt und sich die Grausamkeit der Erkenntnis in ihren Augen breit macht.

Am vierten Tag fuhren wir nach Hause.
Wir blieben einige Tage bei meinen Eltern.
Das half uns sehr, da sie sich viel um Frieda kümmerten und wir uns auch nicht um Alltägliches wie das Essen kümmern mussten.
Sie waren eine riesen Hilfe.
Dann kehrten wir in unsere Wohnung zurück.
Niedergeschlagen, planlos und am schlimmsten: nur zu dritt.
Meiner Frau fiel es schwer, das Wochenbett zu hüten, da sie kein Baby bei sich hatte.
Aber sie musste sich dazu zwingen, ihrer Gesundheit wegen.
Wir bereiteten Enyas Beerdigung vor und versuchten, mit unseren Gefühlen klar zu kommen.

Der Besuch beim Bestattungsunternehmen fiel uns sehr schwer.
In einem Katalog mit Kindersärgen zu blättern und sich für einen zu entscheiden, in dem unsere Tochter beerdigt werden sollte, war eine seelische Qual.
Wir entschieden uns für einen schlichten, weißen Sarg mit goldenen Griffen.
Wir besorgten ein hübsches, blau-weiß-kariertes Sommerkleid für Enya.
Wir kauften es gleich zwei Mal, so konnten wir eines behalten, um es immer wieder ansehen und anfassen zu können.
Dieses Kleid gab eine von Lindas Schwestern bei der Bestatterin ab, damit diese es ihr anziehen konnte.
Als meine Frau von der Schwangerschaft erfuhr, packte sie ein kleines Päckchen, mit dem sie mir von der Schwangerschaft erzählte.
Unter anderem befand sich darin eine kleine, weiße Stoffrassel in Form einer Ente.
Diese bekam sie mit in den Sarg gelegt.
Diese Entenrassel bedeutete mir sehr viel, da ich Enya gerne auch mein "Enchen" nannte, was wie "Entchen" klingt.

![Enyas Grab](/zwerchfellhernie/grab1.png)
<small>Enyas Grab</small>

Am 01.07.2023 war es soweit.
Um 10 Uhr versammelten wir uns in Freidorf mit Freunden und Familien aus ganz Deutschland.
Es konnten nicht alle kommen, die wir gerne gesehen hätten, und doch waren wir von der großen Gruppe Menschen gerührt, die an diesem Tag zu uns fand, um uns zu unterstützen.
Ihr viel zu kleiner Sarg wurde mit vielen Blumen, Kränzen und Geschenken umringt und es flogen Seifenblasen, sodass ihr Grab wunderschön aussah.
Die Sonne schien hell an diesem Tag und brannte heiß in unseren Gesichtern.
Die Beerdigung fand unter freiem Himmel statt.
Als musikalische Begleitung gab es, wie sollte es anders sein, die berührenden Werke der begnadeten Sängerin "Enya".

Zunächst lief "May it be".
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/NmJSQn0dui4?si=32aIu-LGdYuzSnxd&amp;controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<small>

Mornié utúlié (Finsternis ist hereingebrochen)<br/>
Glaube, und du wirst wandeln auf rechtem Pfad<br/>

</small>

Dann trat Bettina nach vorne und trug ein Gedicht vor.
Es war uns wichtig, dass Bettina das Gedicht vorträgt, weil sie die einzige in unserer Familie war, die sie auch kennenlernen durfte.

<small>
Ich denke dein, wenn mir der Sonne Schimmer<br/>
Vom Meere strahlt;<br/>
Ich denke dein, wenn sich des Mondes Flimmer<br/>
In Quellen malt.<br/>
Ich sehe dich, wenn auf dem fernen Wege<br/>
Der Staub sich hebt;<br/>
In tiefer Nacht, wenn auf dem schmalen Stege<br/>
Der Wandrer bebt.<br/>
Ich höre dich, wenn dort mit dumpfem Rauschen<br/>
Die Welle steigt.<br/>
Im stillen Haine geh ich oft zu lauschen,<br/>
Wenn alles schweigt.<br/>
Ich bin bei dir, du seist auch noch so ferne.<br/>
Du bist mir nah!<br/>
Die Sonne sinkt, bald leuchten mir die Sterne.<br/>
O wärst du da!<br/>
</small>
<sup>| Johann Wolfgang von Goethe</sup>

---

Danach hielt ich meine Rede, nach mir meine Frau.
Sie hatte zuvor die Sorge, dass sie kein Wort herausbringen könnte, darum nahm sie eine ihrer Schwestern mit nach vorne, die für sie die Rede vorlesen sollte.
Doch nach wenigen Sätzen fing diese zu weinen an und musste unterbrechen.
An diesem Punkt setzte zu ihrer eigenen Überraschung doch meine Frau ein.
Auf ihre Stärke war ich sehr stolz.
Überhaupt fühlten wir uns an diesem Tag ziemlich gefasst.

Obwohl wir die Inhalte unserer Reden nicht aufeinander abgestimmt haben, kamen wir beide zu dem gleichen Schluss.
Wir beide nahmen diese Erfahrung als Gelegenheit. 
Statt von der unendlichen Last des Schmerzes erdrückt zu werden, sollten wir von der wundervollen Frucht der Erkenntnis kosten.
Wir sollten sehen, welchen Wert das Leben trägt und das es keine Selbstverständlichkeit ist.

<br/>
Nach unseren Reden kam "Only Time".

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7wfYIMyS_dI?si=UZpmAR4_x2HHxVHg&amp;controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Als das Stück beendet war, gingen meine Frau und ich zum Grab und Enya wurde mit ihrem Sarg in ihr Grab herab gelassen.
Während der gesamten Beerdigung saß ein Schmetterling bei uns und ihrem Grab, es handelte sich um einen Admiral.
Schmetterlinge stehen für Transformation und besonders Linda hatte sich die nächsten Monate immer über einen Admiral gefreut, weil er ihr viel Kraft schenkte und sie das Gefühl hatte, Enya käme sie besuchen.
Nun flatterte er herum.
Die beiden Sargträger verbeugten sich und gingen fort.

Meine Frau kniete sich hin und warf Efeublätter und weiße Blütenblätter in das Grab.
Auch ich gab kniend Efeu und Blütenblätter hinzu und auch eine Hand voll Erde.
Dann kamen unsere Familie, Eltern, Geschwister und auch Freunde, fast alle knieten sich hin, trugen etwas bei und sprachen ihre Gebete.
Wer fertig war, kam zu uns und sprach uns sein Beileid aus.
Wir bekamen auch viele rührende Unterstützungsangebote.

Nachdem die Beerdigung zu Ende war, ging ich noch einmal alleine zu ihrem offenen Grab und sprach noch einige Worte.
Auch meine Frau ging noch einmal alleine zum Grab und gab die restlichen Blütenblätter hinein.
Sie kniete sich erneut hin und sprach noch einige Minuten mit ihr.

Danach fuhren wir zu Lindas Eltern.
In ihrem Garten gab es belegte Brötchen und wir konnten uns mit allen noch ein wenig unterhalten.
Im Anschluss fuhr ich noch einmal mit meiner Frau zum Grab, damit wir es uns geschlossen ansehen konnten.

![Gaben für Enya](/zwerchfellhernie/grab2.png)
<small>Gaben für Enya</small>

An diesem Tag waren wir sehr gefasst, aber als wir zwei Tage später in unsere Wohnung zurück kehrten, traf uns die Trauer noch einmal wie ein Schlag.

Die nächsten Monate verbrachten wir mit einem langwierigen Heilungsprozess.
Zunächst gingen wir zur DRK Familienberatung.
Dort konnten wir uns mit einer Fachfrau unterhalten und beratschlagen.

Wir bekamen von meiner Hausärztin und den Nachbarn meiner Eltern die gleiche Telefonnummer: Die Nummer von Antje, einer grandiosen Hebamme, die selber ein Kind verloren hatte und Trauerbegleitung anbot.
Wir haben sie und ihre Art sehr geschätzt und uns aufgehoben und verstanden gefühlt.
Wir konnten mit ihr über alles reden, ohne verurteilt zu werden.
Sie half uns sehr bei der Verarbeitung unserer Trauer und meiner Frau inbesondere bei der Rückbildung.
Wer möchte auch nach dem Tod des eigenen Babys zu einem Rückbildungskurs gehen, wenn dort auch 10 andere Frauen mit ihren lebenden Babys sitzen?

Meine Frau las viele Erfahrungsberichte anderer Eltern mit ähnlichen Schicksalen.
Besonders das Buch "Mein Sternenkind" half ihr.
Es half uns beiden, den Weg der Trauer zu verstehen und uns mit unserem Schicksal nicht alleine zu fühlen.
Denn meist wird einem Menschen nach einem großen Verlust ein halbes Jahr der Trauer zugesprochen, danach sollte das normale Leben sich allerdings wieder eingefunden haben.
Die Praxis zeigt aber, dass dem fast nie so ist. 
Die Trauer kommt in Wellen und inbesondere bei dem Verlust eines Kindes kommt diese Trauer meist noch Jahre später wieder, oder sie wird verdrängt und arbeitet dann im Unterbewusstsein.

Nach einigen Monaten bekam ich auch die Möglichkeit, in Therapie zu gehen.
Diese Möglichkeit habe ich sehr gerne genutzt.

Meine Frau malte einen Baum für Enya an die Wand. 
An dem Baum hängen goldene Schmetterlinge und Bilder von unserer Enya.
So haben wir für sie eine Gedenkecke im Kinderzimmer errichtet, damit sie weder Vergessen noch Verdrängt werden kann.

Zudem haben wir von Lindas Geschwistern eine Kiste geschenkt bekommen, in der wir alle Unterlagen, Bilder, Texte, Karten, ihre Haare, ein zweites Exemplar ihres Grabkleides und ein zweites Exemplar ihrer Ente sowie einige anderer Erinnerungsstücke sammeln. 

Wir fuhren auch noch einmal in das Vivantes Klinikum Neukölln.
Wir nahmen Mareen in den Arm und unterhielten uns mit ihr.
Und wir setzten uns auch einfach noch einmal in das Wartezimmer.
Das alles war für uns sehr angenehm und wir gingen mit einem guten Gefühl nach Hause.

Zudem ließen wir drei identische Grablaternen anfertigen, auf denen die letzte Strophe vom Gedicht ihrer Beerdigung eingraviert ist.
Die erste Laterne kam zu meinen Eltern, die zweite zu meinen Schwiegereltern.
Die letzte Laterne kommt auf Enyas Grab, sobald der Grabstein steht.

Eines Abends besuchte ich mit Frieda meine Eltern.
Linda musste leider zu Hause bleiben, da sie erkältet war.
An diesem Abend wurde uns von all unseren Freunden, unserer Familie und vielen anderen Menschen ein unglaubliches Geschenk gemacht.
Hinter unserem Rücken hatte meine Mutter Geld gesammelt, da sich durch die Beerdigung und auch alle anderen Umstände recht hohe Kosten für uns ergeben hatten.
Uns wurde nicht gesagt, wer etwas gespendet hatte, aber wir wussten, dass von unseren liebsten Menschen sehr viele einen Beitrag geleistet hatten.
Manche hatten ebenfalls unsere Geschichte zum Beispiel auf ihrer Arbeit kundgetan und um Spenden gebeten.
So kamen über 3.000€ zusammen, die uns die entstandenen Kosten sehr viel leichter tragen ließen.
Wir waren zu Tränen gerührt.

Meine Frau schrieb einen Brief an das Universitätsklinikum Mannheim.
In diesem Brief bedankte sie sich ausführlich für die großartigen Mühen, die die Schwestern und Ärzte auf sich genommen hatten.
Sie schrieb auch, dass wir niemandem die Schuld für Enyas Tod geben und sehr hofften, dass es Rosalie und Leonie gut ging.
Und welche Freude kam bei uns auf, als wenige Wochen später ein Brief von Dr. Bayer seinen Weg in unseren Briefkasten fand.
Er schrieb, dass er sich noch sehr gut an uns erinnere und der 16.06.2023 allen in der Neonatologie schmerzhaft in Erinnerung war.
Und auch über Rosalie und Leonie schrieb er: sie hatten sich beide gut entwickelt und waren mittlerweile mit ihren Eltern zu Hause.
Dieser Tag war ein Tag der Freude für uns.

Das Leben geht weiter.
Es unterliegt einer ständigen Veränderung und so haben auch wir uns verändert.
Alles hat sich verändert und es ist nichts wie zuvor.
Wir haben bisher jeden Tag an sie gedacht.
Auch das wird vermutlich nicht ewig so bleiben.
Aber selbst wenn ich mal nicht an sie denke, so ist sie doch stets bei mir.

Enya ist bei mir, wenn ich zur Arbeit fahre.
Sie ist bei mir, wenn ich spazieren gehe.
Sie ist bei mir, wenn ich in den Sternenhimmel blicke oder eine Kerze entzünde.

Ich bin bei dir, du seist auch noch so ferne.<br/>
Du bist mir nah!<br/>
Die Sonne sinkt, bald leuchten mir die Sterne.<br/>
O wärst du da!<br/>

---

[
<u>Meine Rede zur Beerdigung</u>
](/zwerchfellhernie/grabrede_benito.pdf)

[
<u>Die Rede meiner Frau zur Beerdigung</u>
](/zwerchfellhernie/grabrede_linda.pdf)

[
<u>Enyas Lebenstag aus Bettinas Perspektive</u>
](/zwerchfellhernie/enyas_lebenstag_bettina.pdf)

