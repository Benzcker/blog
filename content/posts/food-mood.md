+++ 
date = 2023-01-28T09:00:59+01:00
title = "Essen - Das Geheimnis der guten Laune"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "Ernährung", "Gesundheit" ]
categories = []
externalLink = ""
series = []
+++

An manchen Tagen hat man einfach schlechte Laune.
Alles geht schief, niemand ist da, um zu helfen.
Doch was, wenn das mit unserer Laune gar nichts zu tun haben muss?
Was wäre, wenn die Ursache für schlechte Laune nicht bei Missgeschicken oder Unglücken läge?

![Emotions](/food-mood/smiley.jpg)

<!--more-->

Ernährung ist ein stark umstrittenes Themengebiet.
Es gibt etliche Ernährungsformen, tausende Bücher, Blogs, Videos, Webseiten, usw.
Diskussionen über Ernährung sind oft hitzig und fruchtlos. Jeder beharrt auf seiner Meinung, Gegenargumente werden nicht angehört.

Bei so viel negativer Energie könnte man gleich schlechte Laune bekommen.
Aber das muss nicht sein! Ich werde im Folgenden betrachten, welche Wirkung die Lebensmittel, die wir essen, auf unsere Stimmung haben.

# Wasser
Fangen wir mit etwas an, von dem sich fast alle einig sind, dass es für uns Menschen gut ist: Wasser.
Aber hat Wasser auch positive Wirkungen auf unsere Stimmung?

<cite>

In einer kontrollierten Interventionsstudie von 2014 wurde untersucht, welche Auswirkung das Ändern von Trinkgewohnheiten auf 52 Leute hatte.
Es wurden zwei Gruppen gebildet:
- HIGH: die vor der Studie im Schnitt 2,5 Liter Wasser am Tag tranken
- LOW: die vor der Studie im Schnitt 1 Liter Wasser am Tag tranken

Drei Tage lang tranken die Teilnehmer in der HIGH Gruppe nur noch 1 Liter am Tag, während die Teilnehmer in der LOW Gruppe 2,5 Liter Wasser am Tag tranken.
Mehrere Stimmungsmessungen wurden zu unterschiedlichen Zeitpunkten in der Studie durchgeführt.

Die Einschränkung der Wasserzufuhr in der HIGH Gruppe führte dazu, dass im Vergleich zu vor der Studie der Durst zunahm, die Patienten unzufriedener waren, weniger gelassen, 
weniger positive Emotionen hatten und weniger aktiv waren.

Die Zunahme der Wasserzufuhr in der LOW Gruppe führte dazu, dass die Teilnehmer im Vergleich zu vor der Studie weniger müde waren, 
weniger verwirrt, weniger Durst hatten und weniger schläfrig waren.
[^8]

</cite>

[^8]: N. Pross et al., "Effects of changes in water intake on mood of high and low drinkers ", PLoS One, 2014

# Kohlenhydrate oder Fett?

<cite>

In einer randomisierten, kontrollierten Studie von 2009 wurde getestet, wie übergewichtige und fettleibige Patienten auf eine fettarme und kohlenhydratreiche Ernährung
oder auf eine kohlenhydratarme und fettreiche Ernährung reagieren. Die Versuche liefen über ein Jahr und beobachtete verschiedene Parameter
(Körpergewicht, psychologische Stimmung, Wohlbefinden und kognitive Leistungsfähigkeit).

Im Vergleich zur Gruppe mit der fettreichen Ernährung verbesserte sich die Gruppe mit der kohlenhydratreichen Ernährung in vielen Stimmungszuständen 
(Gemütsverstimmung, Wut, Verwirrung und Depression).
Das Arbeitsgedächtnis verbesserte sich bei beiden Gruppen, die Verarbeitungsgeschwindigkeit blieb bei beiden gleich.
[^1]

</cite>

[^1]: G. D. Brinkworth et al., "Long-term effects of a very low-carbohydrate diet and a low-fat diet on mood and cognitive function", Archives of internal medicine, 2009

## Aber bedeutet das wirklich, dass Fett schlecht ist?

<cite>

In einer anderen randomisierten, kontrollierten Studie von 2012 wurde getestet, ob die Restriktion von Fleisch, Fisch und Geflügel die Stimmung verbessern kann.
Der Gedanke dabei war, dass eine mischköstliche Ernährung deutlich mehr Arachidonsäure enthält, als eine vegetarische.
Arachidonsäure ist eine Fettsäure aus der Gruppe der Omega-6-Fettsäuren und Untersuchungen in der Vergangenheit hatten gezeigt, 
dass sie die Stimmung des Menschen negativ beeinflussen kann.

Die Teilnehmer der Studie waren von Hause aus Mischköstler, wurden aber in drei Gruppen eingeteilt:
- OMN, die weiterhin täglich Fleisch, Fisch und Geflügel aßen,
- FISH, die nur noch Fisch 3-4x pro Woche aßen, und
- VEG, die rein vegetarisch aßen.

Der Hintergrund der Fisch-Gruppe war, dass Fisch Eicosapentaensäure (EPA) und Docosahexaensäure (DHA) enthält, welche die negativen Auswirkungen
der Arachidonsäure reduzieren können. Die Studie lief über 2 Wochen.
Alle drei Gruppen hatten etwa die gleiche Menge Fett in ihrer Ernährung.

Am Ende hatten sich die Stimmungen der Gruppen OMN und FISH nicht geändert, die vieler VEG-Teilnehmer hingegen hatten sich gebessert.
[^2]

</cite>

[^2]: B. L. Beezhold, C.S. Johnston, "Restriction of meat, fish, and poultry in omnivores improves mood: a pilot randomized controlled trial.", Nutrition journal, 2012

Es kommt also nicht zwingend darauf an, wie viel Fett man isst, sondern eher, welches!

### In welchen Lebensmitteln kommt Arachidonsäure vor?

![Arachidonsäure](/food-mood/arachidonic-acid.png)[^3]

[^3]: [plantfadb.org/fatty\_acids/10144](https://plantfadb.org/fatty_acids/10144), abgerufen am 07.01.2023

Arachidonsäure kommt in vielen Lebensmitteln vor, insbesondere in tierischen Produkten.
Leider ist die einzige ausführliche Liste, die ich gefunden habe, auf einer Seite namens [fett-falle.de](https://fett-falle.de). 
Das klingt nicht besonders seriös und die Herkunft der Daten wird auch nicht angegeben, aber es ist die beste Datenquelle, die ich gefunden habe.

<cite>

Die Lebensmittel mit dem höchsten Gehalt an Arachidonsäure sind demnach 
Fischöle (darunter auch Lebertran), Organe wie Herzen, Pankreas, Leber und Hirn, Eier, Aal, Shrimps, Austern und Fleisch (besonders Geflügel, aber auch Schwein und Rind).
[^4]

</cite>

[^4]: [https://fett-falle.de/AATab.html](https://fett-falle.de/AATab.html), abgerufen am 07.01.2023

## Und schlechte Kohlenhydrate?
Wenn schlechtes Fett sich negativ auf die Stimmung auswirkt, tun das schlechte Kohlenhydrate vielleicht ebenfalls.

<cite>

In einem Artikel der wissenschaftlichen Zeitschrift "Experimental and Clinical Psychopharmacology" erschien 2018 eine Studie über die Aufnahme von
Maissirup mit hohem Fruchtzuckergehalt und deren Auswirkung auf kognitive Fähgkeiten und Stimmung bei über 100 jungen und gesunden Erwachsenen.
Die Anspannung in den Patienten stieg immer, wenn sie dachten, sie hätten Zucker konsumiert.
Mit dem Konsum des Sirups stiegen Genauigkeit und Sensitivität in der zu erledigenden Dauerleistungsaufgabe und Fehler wurden weniger.
Es wurden keine anderen Auswirkungen auf Stimmung oder kognitive Leistung festgestellt.
[^5]

</cite>

[^5]: G. E. Giles et al., "Sugar intake and expectation effects on cognition and mood", Exp Clin Psychopharmacol, 2018

Das sieht also nicht so aus, als hätten schlechte Kohlenhydrate auch negative Auswirkungen auf die Stimmung. Allerdings waren in der letzten Studie auch nur
junge und gesunde Menschen und die Messungen wurden lediglich eine halbe Stunde nach Konsum durchgeführt.
Darum sehen wir uns noch mehr an.

<cite>

In einer Meta-Analyse von 2019 wurden 31 Studien mit insgesamt 1259 Patienten untersucht, um heraus zu finden,
ob Kohlenhydrate (speziell Mono- und Disaccharide) eine positive oder negative Wirkung auf unsere Stimmung haben.

Es wurden keine positiven Effekte von Kohlenhydraten auf die Stimmung zu irgendeinem Zeitpunkt nach deren Konsum festgestellt.
Allerdings wurden höhere Müdigkeit und geringere Wachsamkeit im Vergleich zu den Placebogruppen innerhalb der ersten Stunde nach der Einnahme festgestellt.

So zumindest schreiben sie in ihrer Zusammenfassung. Wenn man sich hingegen die ganze Studie ansieht, stellt man fest,
dass die Autoren neben den negativen Effekten durchaus auch viele Studien mit positiven Effekten von Kohlenhydraten betrachtet hatten.
Ihr Ziel war es, die Beziehung zwischen dem Verzehr von Kohlenhydraten und der Stimmung zu untersuchen, indem sie die Ergebnisse aller verfügbaren Studien 
zur Bewertung der Wechselwirkungen zwischen Kohlenhydraten und Stimmung mit Hilfe von Synthesemethoden zusammenfassen und analysieren.
Sie schreiben von Literatur, die beschreibt, dass kohlenhydratreiche Mahlzeiten kognitive Fähigkeiten verbessern können, besonders, wenn die Teilnehmer eher 
anspruchsvolle Aufgaben bewältigen müssen.
Sie betrachten auch Studien, die berichten, dass Kohlenhydrate die Stimmung schützen können, vor Müdigkeit nach hoher kognitiver Anstrengung bewahren, 
Vitalität erhöhen und vor Depression und Stress schützen.

Unter diesem Gesichtspunkt finde ich die Formulierung 
"Die Analyse [...] ergab keinen positiven Effekt von Kohlenhydraten auf irgendeinen Aspekt der Stimmung zu irgendeinem Zeitpunkt nach ihrem Verzehr." in der Zusammenfassung irreführend.
[^6]

</cite>

[^6]: K. Mantantzis et al., "Sugar rush or sugar crash? A meta-analysis of carbohydrate effects on mood ", Neuroscience and biobehavioral reviews, 2019

Wieder einmal sehen wir, dass wissenschaftliche Arbeiten keine einfache Literatur sind und sich häufig widersprechen.
Ich persönlich halte die Angst vor Kohlenhydraten sowie die Angst vor Fett für nicht gerechtfertigt.
Stattdessen sollte sich informiert werden, welche Lebensmittel die guten Varianten von Kohlenhydraten und Fett enthalten und welche Lebensmittel schlechte Varianten beinhalten.

# Pflanzliche Ernährung

<cite>

In einer randomisierten, kontrollierten Studie von 2015 wurde untersucht, 
ob ein planzliches Ernährungsprogramm in einem Unternehmen Depressionen, Angstzustände und Produktivität verbessern kann.
292 Teilnehmer wurden untersucht, die entweder einen BMI >= 25 hatten (bei einer Körpergröße von 1,80m entspricht das mindestens 81kg) oder zuvor mit Diabetes Typ II diagnostiziert wurden.

Es wurden zwei Gruppen gebildet:
Die erste Gruppe hatten 18 Wochen lang wöchentliche Anweisungen, sich vegan zu ernähren, die andere Gruppe bekam keine Anweisungen.

Nach 22 Wochen konnten viele Verbesserungen in der veganen Gruppe im Vergleich zur Kontrollgruppe gemessen werden:
- Verbesserungen bei gesundheitlich bedingten Beeinträchtigungen (in der Arbeit sowie privaten Aktivitäten)
- Verbesserungen bei Depressionen, Angst und Müdigkeit
- Verbesserungen bei emotionalem Wohlbefinden 
- Verbesserungen bei täglichem Funktionieren aufgrund körperlicher Gesundheit
- Verbesserungen bei allgemeiner Gesundheit
[^7]

</cite>

[^7]: U. Agarwal et al., "A multicenter randomized controlled trial of a nutrition intervention program in a multiethnic adult population in the corporate setting reduces depression and anxiety and improves quality of life: the GEICO study", American Journal of Health Promotion, 2015


# Fazit
Für eine gute Stimmung sollten viel Wasser getrunken werden. Fisch, Fleisch, Geflügel, Fischöle, Organe, Eier und Meeresfrüchte hingegen sollten aufgrund ihres hohen Gehaltes
an Arachidonsäure gemieden werden.
EPA und DHA können dem schlechten Fett dieser Produkte entgegenwirken, kommen in Fisch allerdings nicht von Natur aus genügend vor, um die 
stimmungssenkende Wirkung der Arachidonsäure zu hemmen.

Die Auswirkung von Kohlenhydraten auf die Stimmung ist unklar. Einige Studien zeigen positive Auswirkungen auf die Stimmung, andere negative.
Man ist aber aufgrund etlicher anderer gesundheitlicher Vorteile am besten beraten, wenn man auf unverarbeitete Kohlenhydrate wie 
Bananen, Haferflocken und Süßkartoffeln zurückgreift.

Weiterhin zeigt sich, dass eine rein pflanzliche Ernährung viele stimmungsfördernde Wirkungen haben kann.

