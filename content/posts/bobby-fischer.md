+++ 
date = 2022-12-17T12:28:42+01:00
title = "Bobby Fischer - Schachlegende unter Beschuss"
description = "In diesem Post reiße ich die traurige Geschichte der Schachlegende Bobby Fischer an."
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "Schach", "Bobby Fischer" ]
categories = []
externalLink = ""
series = []
+++

Vor kurzem stolperte ich über den Namen Bobby Fischer, als ich mir Schachmemes ansah.


> "If Bobby Fischer invented chess"

![white chess](/bobby-fischer/white-chess.webp) 

Ich verstand das Meme nicht, aber mein Interesse war geweckt.
Also begann ich, mich ein wenig zu Fischer zu belesen. Doch was ich fand, schockierte mich sehr.

<!--more-->

# Wer war Bobby Fischer?
Bobby Fischer war unbestreitbar einer der besten Schachspieler aller Zeiten. 
Auf Wikipedia steht sogar, dass er das höchste Rating aller Zeiten hatte, mit 13 Punkten mehr als Magnus Calsen.
Dazu habe ich allerdings auch an anderen Stellen gelesen, dass Calsen mittlerweile das höchste Rating aller Zeiten hat.
Was in einigen Memes aufgegriffen wird, ist, dass Fischer sich antisemitisch, antiamerikanisch und frauenfeindlich geäußert hat. 
Ich fand allerdings keine Hinweise, dass er sich zur Rassenkunde bekannte. 

Er wurde 1943 in Chicago geboren und wurde der jüngste Schachgroßmeister aller Zeiten.
Sein Buch "My 60 Memorable Games" gilt bis heute als eines der besten Schachbücher überhaupt und "Bobby Fischer Teaches Chess" ist das kommerziell erfolgreichste Schachbuch.
Ein möglicher Grund für den Erfolg des zuletzt genannten Buches ist, dass in diesem Buch vollständig auf die klassische Schachnotation verzichtet und stattdessen auf Bilder mit Pfeilen zurückgegriffen wird.

Fischer verabscheute den Aspekt des Schachspiels, in dem nur auswendig gelernt wird.
Aus diesem Grund erfand er den Spielmodus Schach960, der auch heute noch auf [chess.com](https://chess.com) und [lichess.org](https://lichess.org) gespielt werden kann.
In diesem Modus werden die Figuren der ersten sowie der achten Reihe (also alle Figuren, die keine Bauern sind), zufällig angeordnet, sodass die klassischen Eröffnungen nicht länger möglich sind.
Weiterhin erfand er die Schachuhr, die pro Zug zusätzliche Zeit gibt und somit Zeitnot im Endspiel verhindert.

1972, im Alter von 29 Jahren, gewann er den Weltmeistertitel im Wettkampf gegen den vorherigen Weltmeister Boris Spasski. Diesen Titel behielt er allerdings nur drei Jahre.

# Misshandlungen gegen Fischer
1975 verlor Fischer den Weltmeistertitel. Die Schachweltmeisterschaft 1975 war als Wettkampf zwischen Bobby Fischer und Anatoli Karpow geplant.
Bis zu diesem Zeitpunkt waren die Weltmeisterschaften mit 24 Partien ausgetragen worden, wobei der Weltmeister bei Gleichstand seinen Titel behielt.
Nun hatte die FIDE beschlossen, dass stattdessen der Spieler den Weltmeistertitel bekommen sollte, der als erster sechs Partien gewann.
Fischer weigerte sich und wollte stattdessen, dass der Spieler den Weltmeistertitel bekommen sollte, der als erster zehn Partien gewann. Bei einem 9:9 sollte der Weltmeister seinen Titel behalten.
Da die FIDE und Fischer sich nicht einig wurden, trat Fischer nicht zum Wettkampf an und Karpow wurde kampflos zum Weltmeister ernannt.

Fischer trat nur noch ein einziges Mal auf, zu einem Wettkampf aus 30 Spielen 1992 gegen Boris Spasski, mit dem er privat befreundet war. 
Fischer gewann den Wettkampf und nahm das Preisgeld von \$3,65 Mio. entgegen.
Von diesem Zeitpunkt an wurde Fischer von den US-Behörden mit Haftbefehl gesucht, ihm drohten bis zu 10 Jahre Haft sowie \$250.000 Strafe.
Anklagepunkt war, dass die erste Hälfte des Wettkampfes auf einer Insel statt fand, die dem Chef einer jugoslawischen Bank gehörte. Damit verstieß Fischer gegen das von 
Präsident George W. Bush verkündete Wirschaftsembargo gegen Jugoslawien.
Fischer kehrte nie wieder in die USA zurück.

Zudem wurde schlecht über seine Leistung bei dem Wettkampf geredet. 
Weltmeister Garri Kasparow bezeichnete die Qualität der Partien als niedrig und meinte, Fischer hätte mit diesem Turnier seine eigene Legende zerstört.

Als ob das alles nicht genug wäre, veröffentlichte Fischer schon 1982 das Buch "I was tortured in the Pasadena jailhouse!", in dem er beschrieb, 
wie er aufgrund einer Verwechslung 2 Tage lang von US-Amerikanischen Polizisten gefangen gehalten und gefoltert wurde.
Die Polizisten hatten ihn für einen Bankräuber gehalten.



# Quellen
1. [https://www.reddit.com/r/AnarchyChess/comments/vcu594/if\_bobby\_fischer\_invented\_chess/](https://www.reddit.com/r/AnarchyChess/comments/vcu594/if_bobby_fischer_invented_chess/) 
2. [https://de.wikipedia.org/wiki/Bobby\_Fischer](https://de.wikipedia.org/wiki/Bobby_Fischer) 
3. [https://de.wikipedia.org/wiki/Fischer%E2%80%93Spasski_(Wettkampf\_1992)](https://de.wikipedia.org/wiki/Fischer%E2%80%93Spasski_(Wettkampf_1992)) 

