+++ 
date = 2023-02-18T09:00:59+01:00
title = "Ist Vim den Eliten vorbehalten?"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "IT", "Coding", "Vim" ]
categories = []
externalLink = ""
series = []
+++

Vim ist ein stark umstrittener Text Editor. Viele lieben vim, andere wiederum halten diesen Editor für altmodisch, kompliziert und unpraktisch.

![Exit vim meme](/vim-magic/vim-exit-meme.png) 

Ich werde in diesem Artikel darauf eingehen, was vim so besonders macht, für wen es sich lohnt, vim zu lernen, und wie man am besten anfängt.

<!--more-->

# Was macht die Magie von vim aus?
Es gibt verschiedene Gründe, warum man sich für vim entscheiden könnte.
Da vim ein Terminalprogramm ist, kann man es auf jedem Server verwenden, auch wenn man nur eine ssh Verbindung hat.
Vim ist zudem enorm konfigurierbar, man kann also seinen Texteditor genau auf die eigenen Bedürfnisse anpassen.
Zudem ist vim Open Source, wir können uns also sicher sein, dass keine unserer Daten an den großen Bruder im Westen gesendet werden.

Aber die mit Abstand wichtigste Eigenheit von vim ist seine wunderbare Tastenbelegung.
In jedem anderen Editor tippst du auf den Tasten deiner Tastatur und die jeweiligen Buchstaben erscheinen auf dem Bildschirm.
Aber nicht so in vim! In vim hat jede Taste eine eigene Funktion, mit der man vim sagt, wie man den Text bearbeiten möchte.
Ich übertreibe nicht, wenn ich sage, dass mit dieser Erfindung das Leben von Millionen von Menschen vereinfacht wurde.

Aus diesem Grund kann mit sehr wenigen Tastenschlägen sehr viel erreicht werden. 
Das Kopieren eines Satzes, das Ändern eines Wortes, das Löschen eines Absatzes, alles ist mit 2-3 Tastenschlägen erledigt.
Es muss niemals zur Maus gegriffen werden, denn das würde ebenfalls unnötig Zeit verbrauchen.
Die Funktionen in vim sind so gewählt, dass man keine Fleißarbeit mehr erledigen muss.
Gleiche Wörter im Text suchen und ersetzen kann jeder Texteditor, aber wie sieht es aus,
wenn man 500 Fußnoten im Text hat und diese gerne vom Ende des Dokuments nehmen und stattdessen in Latex-Schreibweise an die korrekte Position im Text einfügen möchte?
Vim hat selbstverständlich keine Taste, die das erledigt. Mit guten Kenntnissen in vim ist diese Aufgabe allerdings in zwei Minuten erledigt - und das ganz ohne Stress.

In der Tat sind die Tastenbelegungen derart sinnvoll belegt, dass sie von Vim Nutzern in vielen anderen Bereichen übernommen wurden.
Egal ob Browser, E-Mail-Client, Musik-Streaming oder Dateimanager, für alles gibt es eine Lösung, wie man vim Tastenbelegungen in diesen Bereich übernehmen kann.

# Für wen ist vim geeignet?
Vim ist als Texteditor selbstverständlich nur für die Leute geeignet, die am Computer Texte schreiben oder bearbeiten.
Wer nur ein paar Spiele spielt und den Computer sonst für nichts weiter nutzt, muss auch kein vim lernen.
Wer den Computer allerdings zum Arbeiten verwendet und häufig Texte bearbeitet, dem lege ich vim definitiv ans Herz.

Programmierer und System-Administratoren sind offensichtliche Berufsgruppen, die mithilfe von vim ihre Produktivität steigern können.
Aber auch Schriftsteller, Blogger und Akademiker können von der Vielfalt dieses unscheinbaren Terminalprogramms profitieren!

# Ist vim schwer zu lernen?
Die kurze Antwort auf diese Frage lautet: Nein!

Wenn man sich ein bis zwei Stunden an einem Nachmittag Zeit nimmt, kommt man bereits an einen Punkt, an dem man mit Vim Texte schreiben und bearbeiten kann.
Nun hängt man natürlich noch an alten Gewohnheiten, dass man beispielsweise einen Absatz mit der Maus markieren und löschen möchte, statt einfach `dap` einzugeben.
Aber nach etwa einer Woche aktiver Benutzung erreicht man den Punkt, an dem viele Funktionen von vim zur Gewohnheit werden. Wenn man dann 
mal wieder in einen anderen Texteditor muss, ärgert man sich, wie aufwändig der Umgang mit diesem Programm doch ist.

# Wo fange ich an?
Man sollte definitiv mit dem `vimtutor` anfangen. Das ist ein Programm, welches bei der Installation von vim direkt mit installiert wird.
Es ist für absolute Anfänger gedacht, wobei es sich auch als Fortgeschrittener lohnt, den Tutor bei Gelegenheit durch zu gehen.
Der vimtutor erklärt alle Basisfunktionen von vim und dauert etwa eine halbe Stunde.
Um vim richtig zu lernen, muss es angewandt werden, also unbedingt die Aufgaben im vimtutor auch tatsächlich erledigen und nicht nur durchlesen.

Hat man den vimtutor ein oder zwei Mal erledigt und vim eine Woche lang benutzt, wird man sich schon sehr sicher in diesem Editor fühlen.
Nun darf man allerdings nicht stagnieren! Einige vim Nutzer bleiben jahrelang bei den Basics.
Stattdessen sollte man seine eigene Arbeitsweise beobachten und recherchieren, wie man die Tätigkeiten, die am meisten Zeit in Anspruch nehmen, in vim effektiver umsetzen kann.
Es wird immer einen einfacheren Weg geben.
Zudem kann es sehr nützlich sein, sich Videos anzusehen, in denen vim Nutzer erklären, welche Funktionen von vim sie besonders häufig nutzen.
Oft genug wird etwas dabei sein, bei dem man sich denkt: "Das ist ja praktisch, hätt' ich das mal schon früher gewusst!"

