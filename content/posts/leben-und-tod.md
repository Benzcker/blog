+++ 
date = 2023-07-17T19:15:00+01:00
title = "Das geheime Leben nach dem Tod"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "Philosophie", "Hermetik" ]
categories = []
externalLink = ""
series = []
+++

<cite>

Wollt ihr mich seh'n, so schließt die Augen,

Wollt ihr mich hör'n, so lauscht dem Wind.

Wollt ihr mich seh'n, schaut in die Sterne,

Wollt ihr mich hör'n, kommt an den Fluss.
[^1]

![Himmelstreppe](/leben-und-tod/himmelstreppe.jpg) 

</cite>

[^1]: Schandmaul, "Euch zum Geleit"

<!--more-->

---

Am Anfang war alles eins und alles nichts.
In einem oft beschriebenen doch unbeschreiblichen Prozess wurde in der kürzesten aller Zeiten aus dem nichts alles.
Und alles war in seiner wundervollen Perfektion großartig. Die Großartigkeit war allerdings nicht greifbar, nicht verständlich, da sie alles war, was war. Darum erschuf alles, was war, alles, was nicht ist.
Somit erschuf es im gleichen Zug die Relativität und Veränderung.
Auf diese Weise kann alles seither sich selbst und seine eigene Großartigkeit erleben.
Alles erschafft sich in jeder Sekunde neu, aus allem, was ist. Nichts ruht, alles ist in Bewegung.
In diesem Prozess - der Veränderung - liegt die Göttlichkeit.

# Woher kommen wir?
Damit ein Mensch zu seiner Existenz kommt, muss er in erster Linie gezeugt werden. 
Damit er genau seine eigene DNA bekommt, spielen unendlich viele Faktoren zusammen. 
Welche DNA der Mann mitgibt hat neben seiner eigenen DNA primär seine Ursachen in der näheren Vergangenheit, wie viel Sport treibt der Vater, wie gesund ernährt er sich, wie lange ist der letzte Geschlechtsverkehr her. 
Das alles verbessert nicht die DNA innerhalb eines einzelnen Spermiums, wohl aber die Anzahl der Spermien und somit die Konkurrenz.
Und zuletzt kommt es darauf an, welches Spermium als erster die Eizelle findet und es schafft, in sie einzudringen.

Bei der Frau ist die ganze Angelegenheit komplizierter und liegt in der weiter entfernten Vergangenheit.
Die Eizellen der Frau werden noch vor ihrer eigenen Geburt entwickelt. Etwa in der 20. Schwangerschaftswoche sind alle Eizellen fertig entwickelt. Zu ihrer Geburt hat eine Frau noch um die 1-2 Millionen Eizellen, wenn sie in die Pubertät kommt sind es meist nur noch 300.000 - 500.000, da sie im Laufe der Zeit degenerieren.
Wird eine Frau also mit beispielsweise Mitte Zwanzig befruchtet, ergibt sich ihr Teil der Kindes-DNA daraus, welche Eizelle noch nicht degeneriert ist, noch nicht zuvor verwendet wurde und in diesem Eisprung in die Eileiter gelassen wird.

Offensichtlich ist die DNA eines Menschen von Milliarden von Faktoren abhängig. Ich habe noch nicht einmal die unfassbar komplexen, notwendigen sozialen Zusammenhänge erläutert, mit denen die beiden Eltern überhaupt erst in einer romatischen Beziehung zueinander finden und ein Kind zeugen.
Und das alles sind auch nur die Grundlagen, mit denen ein Mensch zu seiner DNA kommt.

![DNA](/leben-und-tod/dna.jpg) 

Ein Mensch besteht aber nicht nur aus seiner DNA, sondern auch aus seinen persönlichen Lebenserfahrungen. Welche Freunde lernt er kennen, wie sind seine Eltern? In welchem Land wird er geboren, welche Gewohnheiten nimmt er aus seinem Elternhaus mit?

All diese Faktoren haben eines gemeinsam: Sie sind von dem abhängig, was zu diesem Zeitpunkt ist.
Wir sind die Summe unseres Umfeldes, wobei damit nicht nur das nähere Umfeld gemeint ist. Auch alles andere, was ist, hat aufgrund des Schmetterlingseffekts eine Auswirkung darauf, wer wir werden.
Ein hübsches Gedicht von Goethe hat sicherlich einen enormen Einfluss auf den Verlauf des kalten Krieges gehabt, einfach weil es eine Wirkung auf den Verlauf der Geschichte hatte und somit den Ist-Zustand im 20. Jahrhundert maßgeblich verändert hat.

Je weiter ein Ereignis vom Leben eines Menschen entfernt ist (sowohl örtlich als auch zeitlich), desto schwerer ist die Wirkung auf diesen Menschen nachvollziehbar. Die Wirkung kann scheinbar so gering werden, dass wir meinen könnten, sie wäre nicht existent.
Stattdessen werden nur die Zusammenhänge komplexer.
Was hat die Entscheidung eines russischen Fischers vor 600 Jahren, einen Tag Urlaub zu nehmen, für eine Wirkung auf mein Leben?
Ich weiß es nicht. Entscheidend ist, dass es eine Wirkung auf mein Leben hat.
Wer den Schmetterlingseffekt wirklich verstanden hat, wird auch das gut verstehen.

In der Tat sind wir die Summe aller Ereignisse, die jemals irgendwo passiert sind. Eventuell können einige Ereignisse ausgeschlossen werden, weil deren Standort so weit entfernt lag, dass die Informationen mit der Geschwindigkeit von Licht noch nicht bis zu uns gelangen konnten, andererseits können Informationen mittels Quantenverschränkungen auch mit Überlichtgeschwindigkeit ausgetauscht werden. 
So oder so, in solchen Fällen handelt es sich nur um irrelevante Ereignisse wie der Zusammenstoß zweier Gesteinsbrocken in unserer Nachbargalaxie.
Was aber die Ereignisse auf dieser Erde angeht, können wir sicher sein, dass wir die Summe jedes bisher geschehenen Ereignisses sind.

# Wohin gehen wir?
So wie alles auf dieser Erde eine Wirkung auf uns hat, so haben selbstverständlich auch wir eine ebenso starke Wirkung auf alles andere.
In manchen Fällen ist das offensichtlich: Meine Kinder wurden von mir gezeugt, meine Freunde werden von meinen abgedrehten Theorien überzeugt, Bekannte lesen Bücher, die ich ihnen empfohlen habe.
Welche Wirkung habe ich nun auf den japanischen Kirschbaum in Okinawa?
Vermutlich keine direkte. Beispielsweise beeinflusse ich aber die Denkweise, Kultur und Wirtschaft in Deutschland.
Über diese widerum könnte sich der japanische Gärtner bewusst sein oder jemand in seinem Umfeld und dieses Wissen beeinflusst deren Lebensweise. Diese Lebensweise beeinflusst, in welcher Art er sich um den Kirschbaum kümmert oder welche anderen Entscheidungen er trifft (beispielsweise das Einpflanzen einer deutschen Pflanze, die dem Kirschbaum Konkurrenz macht, weil Deutschland aktuell recht beliebt bei den Japanern ist).

Man könnte meinen, die Einflüsse der Vergangenheit gingen mit der Zeit verloren, doch dem ist nicht so.
Zusätzlich zu dem Massen- und Energieerhaltungssatz haben wir in der Physik auch den etwas weniger bekannten Informationserhaltungssatz. Dieses Prinzip basiert auf dem zweiten Hauptsatz der Thermodynamik und der Quantenmechanik.
Das Stichwort lautet Unitarität und besagt, dass die Entwicklung eines quantenmechanischen Systems stets umkehrbar ist und keine Informationen verloren gehen.

Zudem könnten diese Einflüsse recht unverhersehbar und willkürlich erscheinen. In ihrer Essenz bleiben Ursache und Wirkung jedoch gleich.
Ein Akt der Liebe wird nur mehr Liebe in der Welt schaffen, ein Akt der Angst schafft wiederum weitere Angst.
Dies ist wunderbar in der "Hermetica" von Hermes Trismegistos (oder den hinter diesem Namen versteckten Autoren) beschrieben. 
Es handelt sich um die sogenannten Spiegelgesetze, dem Prinzip der Entsprechung. "Wie oben, so unten. Wie innen, so außen."
Es besagt, dass es eine Harmonie und Ähnlichkeit zwischen den verschiedenen Ebenen der Existenz gibt.
In der modernen Psychologie reden wir von dem Resonanzgesetz. Was wir nach außen abgeben, werden wir auch von außen erfahren. Das liegt nicht einfach daran, dass wir die Menschen um uns herum dazu bringen, uns eine ähnliche Seite von ihnen zu zeigen.
Es liegt daran, dass wir die Menschen tatsächlich verändern, durch das, was wir sind.

![Hermes Trismegistos](/leben-und-tod/hermes-trismegistos.jpg) 

Und wir verändern nicht nur Menschen, auch Tiere, Pflanzen, Steine, Gewässer, Asteroiden, Sonnen.
Wir verändern alles durch unsere Existenz.
Wir senden in die Welt, was wir sind, und die Welt wird nach dem Prinzip der Entsprechung ein Stück mehr zu uns.

Wenn also alles unsere Form annimmt, wir in allem stecken, das da ist.
Wenn wir in jedem Menschen und in jedem Blatt unsere Spuren hinterlassen, dann hatte Schandmaul recht.

> Wollt ihr mich seh'n, so schließt die Augen, 
> Wollt ihr mich hör'n, so lauscht dem Wind.
> Wollt ihr mich seh'n, schaut in die Sterne,
> Wollt ihr mich hör'n, kommt an den Fluss.

# Was ist mit Sternenkindern?
Ein Neugeborenes, das stirbt, hatte wenig bis keine Gelegenheit, sich selbst in die Welt einzubringen.
Es konnte keine Freunde finden, keine Bücher schreiben.
Es konnte niemanden heiraten und keine Kinder bekommen.

Und doch ist seine Existenz nicht leugenbar, unmöglich aus der Geschichte zu streichen.
Viele haben geweint: Eltern und Großeltern, Tanten und Onkel, Freunde und Bekannte.
Ärzte haben einen Niederschlag erlitten, ein Grab wurde ausgehoben.

All das und vieles mehr ist nur wegen eines verstorbenen Kindes passiert.
Ist das weniger, als ein durchschnittlicher Mensch in seinem Leben an Ereignissen verursacht?
Vermutlich schon. 
Das bedeutet allerdings nicht, dass dieses Kind eine geringe Auswirkung auf die Welt hat.
Auch in diesem Fall kommt der Schmetterlingseffekt zu tragen.
Die Auswirkungen der bloßen Existenz eines Neugeborenen nehmen schnell derartige Maße an, dass sie in ihrem Ausmaß nicht von den Auswirkungen eines vollen Menschenlebens zu unterscheiden ist.

> "Deine kleinen Füße hatten nie die Gelegenheit, die Erde zu berühren, und doch hast du Spuren hinterlassen."

Ein Sternenkind hat ebenso viel Macht in der Welt, wie jeder andere Mensch auch. 
Es kann Bücher beeinflussen, Berge verschieben, Sonnen verändern. Warum heißt es wohl "Sternenkind"? 
Oder es kann einen Vater dazu bringen, einen Artikel über das geheime Leben nach dem Tod zu schreiben. 

