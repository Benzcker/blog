
+++ 
date = 2024-03-30T07:00:00+01:00
title = "Warum es bei Ostern nicht um die Wiederauferstehung geht"
description = ""
slug = ""
authors = [ "Benito Zenz" ]
tags = [ "Christus", "Hermetik", "Philosophie" ]
categories = []
externalLink = ""
series = []
+++

Anlässlich des anstehenden Osterfestes habe ich mich erneut dem achten Teil von Thorwald Dethlefsens Vortragszyklus gewidmet: Gedanken zum Ostermysterium.
Thorwald Dethlefsen war ein Pionier der spirituellen Entwicklung in Deutschland und seine Werke verdienen die große Aufmerksamkeit, die sie bekommen.
Ich habe in diesem Artikel die Grundaussagen dieses Vortrags zusammengefasst und um einige meiner eigenen Gedanken erweitert.
Dennoch rate ich jedem, den Vortrag "Gedanken zum Ostermysterium" von Thorwald Dethlefsen entweder zu hören oder die gedruckte Form zu erwerben.
Es ist jeden Cent wert.

![Jesus am Kreuz](/ostern/christ.jpg)

<!--more-->

Im Zeichen des Ostermysteriums offenbart sich eine Zeit der Reflexion und der tiefen spirituellen Bedeutung, in der wir uns mit der Essenz von Einheit und Polarität auseinandersetzen.
Über Generationen hinweg hat dieses festliche Ereignis nicht nur christliche Gläubige, sondern auch Menschen unterschiedlicher Glaubensrichtungen und philosophischer Überzeugungen in seinen Bann gezogen.
In dieser Betrachtung möchte ich die vielschichtigen Aspekte des Ostermysteriums erkunden und seine Parallelen zu anderen Religionen, der Alchemie und sogar dem mythischen Sündenfall beleuchten.

## Einheit und Vielheit
Wir fangen mit dem Unterschied von Form zu Inhalt an.
Der Inhalt ist das, was wirklich ist.
Die Form hingegen ist nur die Illusion, die durch den Inhalt entsteht.
Es handelt sich bei einer Form um den Ausdruck eines Inhalts, allerdings nicht um den Inhalt selber.
Die Welt der Materie ist die Welt der Formen und somit polar, also gespalten.
Sie spiegelt den Geist, die Einheit.

Die Erleuchtung ist der Übergang aus der Polarität zur Einheit, also von der Form zum Inhalt.
Problematisch ist, dass wenn wir das beschreiben wollen, wir nur Formen nutzen können.
Und fast jede Religion kommt irgendwann an den Punkt, an dem die Bilder, mit denen die Erleuchtung beschrieben wird, nur noch konkret verstanden wird.
Dann sehen die Anhänger nur noch die Form und nicht den Inhalt und wir haben eine exoterische Religion.

Um die Einheit zu beschreiben, müssen wir uns Paradoxien bedienen, da wir in der Polarität gefangen sind und jede nichtparadoxe Aussage etwas aus der Einheit ausschließen würde.
Dies würde allerdings nicht der Wahrheit entsprechen.
Lasst es uns dennoch versuchen.

Bei der Schöpfung zerfällt die Einheit in die Vielheit, bleibt aber dennoch erhalten.
Darum also die Frage: Ist die Welt der Polarität, also der Vielheit, in der Einheit oder außerhalb von ihr?
Die Einheit kennt weder Zeit noch Raum, da diese Abgrenzungen bedeuten, die in der Einheit nicht vorkommen.
Darum kann die Welt der Polarität nicht in der Einheit sein.
Sie kann aber auch nicht außerhalb der Einheit sein, da es außerhalb der Einheit nichts gibt.
Die einzige Schlussfolgerung daraus muss lauten, dass es die Polarität, die Schöpfung, gar nicht geben kann.
Sie ist eine reine Illusion und diese Einsicht ist die Befreiung.

Wir kommen aus der Einheit, gehen in die Vielheit und kehren dann zurück in die Einheit.
Die Einheit erstirbt in die Vielheit, das Licht geht in die Finsternis und Gott wird Mensch.
Durch das Essen der Frucht vom Baum der Erkenntnis kam der Mensch in die Polarität, denn zur Erkenntnis benötigen wir Polarität, die Möglichkeit, zu unterscheiden.
Dieser Baum war übrigens ein Feigenbaum und kein Apfelbaum.
Adam und Eva bedeckten ihre Nacktheit mit seinen Blättern, mit Feigenblättern.

## Dreigliedrigkeit
Gott wird dadurch gefunden, dass die Gegensätze der Polarität geeint werden, in der Alchemie heißt es "solve et coagula", löse und binde.
Die oft verwendete Dreigliedrigkeit besteht aus der Polarität zusammen mit der Vereinigung der Gegensätze.

Wir haben zunächst den Körper, die Verfestigung.
Ihm gegenüber steht sein Gegenpol: die Seele, die Ausdehnung.
Dazwischen befindet sich Geist, die Balance.

So gut wie jede andere spirituelle Dreigliedrigkeit findet sich in diesen Bedeutungen wieder.
- Der Körper ist in der Astrologie der Saturn, bei den Griechen ist es Chronos, der Gott der Zeit, in der nordischen Mythologie Loki, in der Alchemie das Salz.
- Die Seele ist Jupiter oder bei den Griechen Zeus, bei den Nordmännern ist es Thor, bei den Alchemisten der Schwefel.
- Der Geist ist der Merkur, oder auch Hermes, Odin oder das Quecksilber.

Bemerkenswert ist, dass Zeus, also die Seele, bei den Griechen als mächtigster Gott angesehen wird, dessen Gegenpol Chronos ist, die Zeit bzw. der Körper.
Dazwischen steht Hermes, der Geist.
Die Smaragdtafel des Hermes Trismegistos ist eines der wichtigsten spirituellen Werke aller Zeiten.

Zeus steht analog zu Thor, Chronos ist Loki und Hermes ist Odin.
Diese Zuordnung mag manche überraschen, sie trifft aber unwidersprüchlich zu, auch wenn wir beispielsweise über die Wochentage gehen.
- Mittwoch ist der Tag von Merkur und Hermes, denn es ist die Mitte der Woche (wenn man die Woche traditionell bei Sonntag beginnt). Im Englischen heißt dieser Tag "Wednesday", was von "Wodans Day" kommt, und Wodan ist Odin.
![Hermes und Odin](/ostern/hermes-odin.png)
<small>
[Souls on the Banks of the Acheron](https://commons.wikimedia.org/wiki/File:Adolf_Hir%C3%A9my-Hirschl_-_Die_Seelen_am_Acheron_-_942_-_%C3%96sterreichische_Galerie_Belvedere.jpg#/media/File:Adolf_Hir%C3%A9my-Hirschl_-_Die_Seelen_am_Acheron_-_942_-_%C3%96sterreichische_Galerie_Belvedere.jpg)
/
[Odin, in his guise as a wanderer (Georg von Rosen)](https://commons.wikimedia.org/wiki/File:Georg_von_Rosen_-_Oden_som_vandringsman,_1886_(Odin,_the_Wanderer).jpg).jpg)
</small>
- Donnerstag ist der Tag von Jupiter und Zeus, aber auch von Thor, denn es ist "Donars Tag", und Donar ist Thor.
![Zeus und Thor](/ostern/zeus-thor.png)
<small>
[The "Golden Man" Zeus statue](https://commons.wikimedia.org/wiki/File:Schloss_Rastatt-Goldener_Mann.jpg#/media/File:Schloss_Rastatt-Goldener_Mann.jpg)
/
[Thor's Fight with the Giants (Mårten Eskil Winge)](https://commons.wikimedia.org/wiki/File:M%C3%A5rten_Eskil_Winge_-_Tor%27s_Fight_with_the_Giants_-_Google_Art_Project.jpg)
</small>
- Samstag ist der Tag von Saturn ("Saturday") und wird auch bei den Griechen traditionell Chronos und in der nordischen Mythologie Loki zugeordnet.
![Chronos und Loki](/ostern/chronos-loki.png)
<small>
[Chronos and His Child (Giovanni Francesco Romanelli)](https://commons.wikimedia.org/wiki/File:Romanelli_Chronos_and_his_child.jpg)
/
[Loki's flight to Jötunheim (W. G. Collingwood)](https://commons.wikimedia.org/wiki/File:Loki%27s_flight_to_J%C3%B6tunheim.jpg)
</small>

Wir sehen auch, dass Thor (Seele) und Loki (Körper) als Brüder dargestellt werden, wenn auch nicht biologische Brüder.
Es ist eine Bruderschaft voller Streitigkeiten und Konflikten.

Und wir sehen auch, dass Odin zu seiner Weisheit kam, indem er am Weltenbaum hin, vom Speer verwundet, so wie Jesus von Nazaret zum Christus wurde, indem er am Kreuze hing, von einer Lanze verwundet.
Nachdem Odin die Weisheit empfing und den Völkern weitergab, "ging er dahin, von wo er gekommen war", ebenfalls eine klare Analogie zu dem Weg des Christus, dem Weg aus der Einheit in die Polarität und wieder zurück zur Einheit.
![Jesus und Odin](/ostern/jesus-odin.png)
<small>
[Die Passion Jesu gipfelt in dessen gewaltsamem Tod am Kreuz auf Golgatha (Albrecht Altdorfer)](https://commons.wikimedia.org/wiki/File:Albrecht_Altdorfer_-_Christus_am_Kreuz_mit_Maria_und_Johannes_(Gem%C3%A4ldegalerie_Alte_Meister_Kassel).jpg)
/
[Odin (Jake Powning)](https://www.pinterest.co.uk/pin/433541901616663276/)
</small>

## Viergliedrigkeit
Wir müssen nun noch die Dreiheit zur Vierheit erweitern.
Das Symbol der Einheit ist ein Punkt ohne Dimensionen, das Symbol der Polarität ist eine Gerade.
Die weiteste Entwicklungsmöglichkeit für die Polarität ist die Polarität, durch die zwei sich schneidende Linien denkbar sind.
Es entsteht ein Kreuz, das Symbol der Vierheit und der stofflichen Verwirklichung der Polarität.
Die Polarität repräsentiert nicht nur einfach zwei entgegengesetzte Pole, sondern auch eine höhere Ebene der Interaktion und Verbindung zwischen diesen Polen.
Wo sich beide Linien schneiden entsteht ein Punkt, die Offenbarung der Einheit in der Polarität.

Christus ist das Symbol für die Ganzheit - ein Widerspruch, da ja Gott die Ganzheit ist und der Sohn den Vater auf Erden vertritt.
Christus nimmt in seiner Menschwerdung die Rolle des Sohnes ein und muss Sterben, um wieder Vater zu werden, so wie auch der Vater sterben musste, um Sohn zu werden.

An Weihnachten feiern wir die Geburt Christi und den Tod Gottes.
Das Licht geht in die Finsternis und aus der Einheit wird Polarität, Gott wird zu Form.
Ostern ist die Umkehrung dieses Prozesses, in der Gott aus der Form entsteht und in die ewige Einheit übergeht.
Der Leidensweg dazwischen ist auch unser Weg und er wird uns von Christus gezeigt.
Obwohl der Weg aus dieser Welt führt, bedeutet er das Gegenteil von Weltflucht, nämlich Hingabe.

Jesus überwindet den Tod, indem er sich damit vereinigt.
Nur die Vereinigung der Gegensätze führt zum Heil, Kampf kann Spaltung nicht überwinden.
Am Karfreitag, als die Römer Jesus holen, zieht Petrus sein Schwert und schlägt dem römischen Bevollmächtigten das rechte Ohr ab.
Jesus weist daraufhin Petrus an, sein Schwert zurück zu stecken.
"Diesen Kelch hat mein Vater für mich bestimmt. Muss ich ihn dann nicht trinken?"

Nach Jesus Gefangennahme ist nur einer seiner Jünger bei ihm: Johannes, da nur er von Jesus eingeweiht wurde.
Am Karsamstag kamen noch Josef von Arimathäa und Nikodemus zu Jesus, nahmen ihn vom Kreuze und salbten ihn.
Dann brachten sie ihn in das Privatgrab von Josef von Arimathäa.
Das war für Josef etwas sehr besonderes, denn auf diese Weise empfing er das Sakrament auf einzigartige, wörtliche Weise.
Er empfing den wahren Leib Christi in seinem Grab und empfing auch das wahre Blut Christi, das er noch am Kreuz in einem Kelch auffängt.
Laut Überlieferungen war dieser Kelch aus einem Stein aus Luzifers Krone gemacht und über Umwege in den Besitz von Josef von Arimathäa gekommen.
Aus diesem Kelch nahm Jesus noch zwei Tage zuvor selbst das Abendmahl.
Später wurde dieser Kelch zum Heiligen Gral.

Das Mysterium der Auferstehung wird von Thorwald Dethlefsen nicht ausführlich besprochen, da es sich vollkommen der menschlichen Sprache entzieht.
Es ist das Erwachen am achten Tag in der Einheit, etwas vollkommen Unvorstellbares, die eigentliche Freiheit.
Auch ich kann dazu zu diesem Zeitpunkt nicht mehr sagen.

## Das Kreuz
Abschließend noch zum Kreuz:
Das Kreuz ist das Symbol der Vierheit, der entfalteten Polarität.
Man kann grundsätzlich jede Polarität für die Balken einsetzen, aber in erster Linie sind es Raum und Zeit.
Denn dies sind die Täuschungen, durch die unsere Welt geschaffen wird und in die wir durch unsere fünf Sinnesorgane gebunden sind.
Der leidende Jesus repräsentiert den durch fünf Wunden an die Welt der Materie gebundenen Menschen.

Doch Jesus befreit sich nicht vom Kreuz, sondern an dem und durch das Kreuz.
Wo sich die beiden Achsen schneiden und der Punkt entsteht, eint sich die Polarität und die Sünde, die die Vielheit ist, wird überwunden.
Die Befreiung von der Polarität und der Welt passiert nur durch die Polarität, nicht durch die Abwendung von ihr.

Im vierten Buch Mose wird beschrieben, wie Gott Schlangen gegen die Isrealiten schickt.
Das Volk ging zu Moses und gestand ihre Sünde.
Moses legte Fürsprache beim Herrn ein und dieser antwortete Moses, er solle eine eherne (bronzene) Schlange anfertigen und sie an einer Stange aufrichten.
Jeder, der sie ansah, blieb am Leben.

![Michelangelos Darstellung der Befreiung der Israeliten von der Schlangenplage durch die Erschaffung der Bronzeschlange](/ostern/michelangelo_buonarroti.png)
<small>
[Michelangelos Darstellung der Befreiung der Israeliten von der Schlangenplage durch die Erschaffung der Bronzeschlange](https://commons.wikimedia.org/wiki/File:Michelangelo_Buonarroti_024.jpg)
</small>

Dies ist die exakte Umkehrung des Sündenfalls, bei dem die Schlange von oben aus dem Baum nach unten kam.
Es lohnt sich einen Blick auf den Stab des Hermes zu werfen, der für Immunität, Heilung und die Verbindung gegensätzlicher Kräfte steht.

![Caduceus](/ostern/caduceus.png)

Die Kreuzigung ist ebenfalls die Umkehrung des Sündenfalls.
Hier erleben wir die Aufrichtung von Jesus, der die aufsteigende Schlange symbolisiert.

Um das Ganze zu vollenden, sehen wir uns noch eine letzte Überlieferung an.
Adam soll einen Zweig vom Baum der Erkenntnis mitgenommen haben.
Dieser Zweig wurde später zum Stab von Moses, mit dem er das Meer teilte und an dem er die eherne Schlange aufrichtete.
Dieser Stab wurde dann zum Balken im Tempel von Salomo und kam dann zum Zimmermann Joseph.
Judas erwarb von ihm den Stab und gab ihn den römischen Soldaten, die daraus das Kreuz fertigten, an dem Jesus gekreuzigt wurde.
Und so wurde aus dem Baum der Erkenntnis, dem ersten Paradiesbaum, der Baum des Lebens, der zweite Baum im Paradies.

Ich möchte noch einmal betonen, wie fruchtbar die Werke von Thorwald Dethlefsen sind.
Wer diese Geschichte breiter ausgeführt hören und zudem den genauen Ablauf der Karwoche verstehen möchte, dem empfehle ich noch einmal wärmstens den achten Teil seines Vortragszyklus.

Der Fall Adams schloss die ganze Menschheit ein, da Adam in jedem Menschen enthalten ist.
Ebenso ist der zweite Adam, Christus, in jedem Menschen enthalten.
Und so kann jeder den Weg Jesu in seiner eigenen Seele reproduzieren.
Mögen wir die Schlange in uns aufrichten, um die Gegensätze zu vereinen und die göttliche Einheit zu erfahren, die in jedem von uns ruht.
So können auch wir Ostern wahrhaftig erleben.

